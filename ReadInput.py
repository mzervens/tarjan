﻿# -*- coding: utf-8 -*-
'''
[[],
     [ 2, 3, 4, 5, 7, 9, 10], 
     [ 1, 3, 4, 6, 7, 8, 10],
     [ 10, 1, 4, 2, 6, 7, 8],
     [ 2, 1, 3, 6, 9],
     [ 1, 2, 7, 9],
     [ 10, 2, 3, 4, 7, 8, 9],
     [ 1, 6, 5, 3, 2, 8],
     [ 10, 2, 3, 6, 7, 9],
     [ 1, 4, 5, 6, 8],
     [ 3, 6, 8, 2, 1]]
'''
def readInput(file_path):

    GT = [[],]
    adjList = {}
    edgW = {}

    F = open(file_path, "r") 

    lines = F.readlines()
    line = "".join(lines)
    F.close()

    parts = line.split()

    n = parts[0]
    iter = 1

    while iter < len(parts):
        n1 = int(parts[iter])
        n2 = int(parts[iter+1])
        w = int(parts[iter+2])
        iter = iter + 3
        
        if not n1 in adjList:
            adjList[n1] = {}
        if not n2 in adjList:
            adjList[n2] = {}

        if not n2 in adjList[n1]:
            adjList[n1][n2] = 1
        if not n1 in adjList[n2]:
            adjList[n2][n1] = 1

        edgW[str(n1)+" "+str(n2)] = w
        edgW[str(n2)+" "+str(n1)] = w

    sortKeys = sorted(adjList.keys())

    for key in sortKeys:
    #for key in adjList:
        neighb = adjList[key]
        ll = []
        for neib in neighb:
            ll.append(neib)
        GT.append(ll)

    return [GT, edgW]
    