﻿# -*- coding: utf-8 -*-

from copy import deepcopy

from OutputResult import outputResult

def printList(ll):
    for x in range(len(ll)):
        print(ll[x])

def getWeights():
    edgW = {}
    edgW[str(1)+" "+str(2)] = 54
    edgW[str(2)+" "+str(1)] = 54
    edgW[str(1)+" "+str(3)] = 472
    edgW[str(3)+" "+str(1)] = 472
    edgW[str(1)+" "+str(4)] = 676
    edgW[str(4)+" "+str(1)] = 676
    edgW[str(1)+" "+str(5)] = 160
    edgW[str(5)+" "+str(1)] = 160
    edgW[str(1)+" "+str(6)] = 316
    edgW[str(6)+" "+str(1)] = 316
    edgW[str(1)+" "+str(7)] = 27
    edgW[str(7)+" "+str(1)] = 27
    edgW[str(1)+" "+str(11)] = 611
    edgW[str(11)+" "+str(1)] = 611
    edgW[str(1)+" "+str(14)] = 164
    edgW[str(14)+" "+str(1)] = 164
    edgW[str(1)+" "+str(22)] = 911
    edgW[str(22)+" "+str(1)] = 911
    edgW[str(1)+" "+str(28)] = 808
    edgW[str(28)+" "+str(1)] = 808
    edgW[str(1)+" "+str(38)] = 473
    edgW[str(38)+" "+str(1)] = 473
    edgW[str(1)+" "+str(39)] = 35
    edgW[str(39)+" "+str(1)] = 35
    edgW[str(1)+" "+str(46)] = 776
    edgW[str(46)+" "+str(1)] = 776
    edgW[str(1)+" "+str(49)] = 35
    edgW[str(49)+" "+str(1)] = 35
    edgW[str(2)+" "+str(11)] = 418
    edgW[str(11)+" "+str(2)] = 418
    edgW[str(2)+" "+str(15)] = 303
    edgW[str(15)+" "+str(2)] = 303
    edgW[str(2)+" "+str(16)] = 859
    edgW[str(16)+" "+str(2)] = 859
    edgW[str(2)+" "+str(18)] = 866
    edgW[str(18)+" "+str(2)] = 866
    edgW[str(2)+" "+str(19)] = 214
    edgW[str(19)+" "+str(2)] = 214
    edgW[str(2)+" "+str(37)] = 209
    edgW[str(37)+" "+str(2)] = 209
    edgW[str(2)+" "+str(38)] = 805
    edgW[str(38)+" "+str(2)] = 805
    edgW[str(3)+" "+str(16)] = 366
    edgW[str(16)+" "+str(3)] = 366
    edgW[str(3)+" "+str(18)] = 681
    edgW[str(18)+" "+str(3)] = 681
    edgW[str(3)+" "+str(22)] = 620
    edgW[str(22)+" "+str(3)] = 620
    edgW[str(3)+" "+str(30)] = 670
    edgW[str(30)+" "+str(3)] = 670
    edgW[str(3)+" "+str(31)] = 642
    edgW[str(31)+" "+str(3)] = 642
    edgW[str(3)+" "+str(32)] = 166
    edgW[str(32)+" "+str(3)] = 166
    edgW[str(4)+" "+str(5)] = 675
    edgW[str(5)+" "+str(4)] = 675
    edgW[str(4)+" "+str(6)] = 859
    edgW[str(6)+" "+str(4)] = 859
    edgW[str(4)+" "+str(8)] = 258
    edgW[str(8)+" "+str(4)] = 258
    edgW[str(4)+" "+str(21)] = 77
    edgW[str(21)+" "+str(4)] = 77
    edgW[str(4)+" "+str(34)] = 232
    edgW[str(34)+" "+str(4)] = 232
    edgW[str(4)+" "+str(38)] = 4
    edgW[str(38)+" "+str(4)] = 4
    edgW[str(5)+" "+str(14)] = 564
    edgW[str(14)+" "+str(5)] = 564
    edgW[str(5)+" "+str(25)] = 148
    edgW[str(25)+" "+str(5)] = 148
    edgW[str(5)+" "+str(30)] = 97
    edgW[str(30)+" "+str(5)] = 97
    edgW[str(5)+" "+str(35)] = 182
    edgW[str(35)+" "+str(5)] = 182
    edgW[str(5)+" "+str(38)] = 80
    edgW[str(38)+" "+str(5)] = 80
    edgW[str(5)+" "+str(41)] = 229
    edgW[str(41)+" "+str(5)] = 229
    edgW[str(6)+" "+str(7)] = 576
    edgW[str(7)+" "+str(6)] = 576
    edgW[str(6)+" "+str(10)] = 25
    edgW[str(10)+" "+str(6)] = 25
    edgW[str(6)+" "+str(25)] = 546
    edgW[str(25)+" "+str(6)] = 546
    edgW[str(6)+" "+str(28)] = 104
    edgW[str(28)+" "+str(6)] = 104
    edgW[str(6)+" "+str(35)] = 923
    edgW[str(35)+" "+str(6)] = 923
    edgW[str(6)+" "+str(38)] = 365
    edgW[str(38)+" "+str(6)] = 365
    edgW[str(6)+" "+str(39)] = 269
    edgW[str(39)+" "+str(6)] = 269
    edgW[str(6)+" "+str(41)] = 481
    edgW[str(41)+" "+str(6)] = 481
    edgW[str(7)+" "+str(13)] = 635
    edgW[str(13)+" "+str(7)] = 635
    edgW[str(7)+" "+str(17)] = 766
    edgW[str(17)+" "+str(7)] = 766
    edgW[str(7)+" "+str(19)] = 541
    edgW[str(19)+" "+str(7)] = 541
    edgW[str(7)+" "+str(20)] = 717
    edgW[str(20)+" "+str(7)] = 717
    edgW[str(7)+" "+str(26)] = 162
    edgW[str(26)+" "+str(7)] = 162
    edgW[str(7)+" "+str(46)] = 63
    edgW[str(46)+" "+str(7)] = 63
    edgW[str(7)+" "+str(47)] = 374
    edgW[str(47)+" "+str(7)] = 374
    edgW[str(7)+" "+str(49)] = 93
    edgW[str(49)+" "+str(7)] = 93
    edgW[str(8)+" "+str(9)] = 575
    edgW[str(9)+" "+str(8)] = 575
    edgW[str(8)+" "+str(22)] = 472
    edgW[str(22)+" "+str(8)] = 472
    edgW[str(8)+" "+str(24)] = 258
    edgW[str(24)+" "+str(8)] = 258
    edgW[str(8)+" "+str(42)] = 465
    edgW[str(42)+" "+str(8)] = 465
    edgW[str(8)+" "+str(43)] = 121
    edgW[str(43)+" "+str(8)] = 121
    edgW[str(9)+" "+str(14)] = 488
    edgW[str(14)+" "+str(9)] = 488
    edgW[str(9)+" "+str(16)] = 61
    edgW[str(16)+" "+str(9)] = 61
    edgW[str(9)+" "+str(23)] = 279
    edgW[str(23)+" "+str(9)] = 279
    edgW[str(9)+" "+str(33)] = 402
    edgW[str(33)+" "+str(9)] = 402
    edgW[str(9)+" "+str(36)] = 849
    edgW[str(36)+" "+str(9)] = 849
    edgW[str(9)+" "+str(37)] = 15
    edgW[str(37)+" "+str(9)] = 15
    edgW[str(9)+" "+str(42)] = 620
    edgW[str(42)+" "+str(9)] = 620
    edgW[str(10)+" "+str(12)] = 875
    edgW[str(12)+" "+str(10)] = 875
    edgW[str(10)+" "+str(21)] = 465
    edgW[str(21)+" "+str(10)] = 465
    edgW[str(10)+" "+str(22)] = 422
    edgW[str(22)+" "+str(10)] = 422
    edgW[str(10)+" "+str(35)] = 487
    edgW[str(35)+" "+str(10)] = 487
    edgW[str(10)+" "+str(36)] = 44
    edgW[str(36)+" "+str(10)] = 44
    edgW[str(10)+" "+str(50)] = 695
    edgW[str(50)+" "+str(10)] = 695
    edgW[str(11)+" "+str(19)] = 965
    edgW[str(19)+" "+str(11)] = 965
    edgW[str(11)+" "+str(24)] = 353
    edgW[str(24)+" "+str(11)] = 353
    edgW[str(11)+" "+str(33)] = 862
    edgW[str(33)+" "+str(11)] = 862
    edgW[str(11)+" "+str(40)] = 931
    edgW[str(40)+" "+str(11)] = 931
    edgW[str(11)+" "+str(45)] = 422
    edgW[str(45)+" "+str(11)] = 422
    edgW[str(11)+" "+str(47)] = 525
    edgW[str(47)+" "+str(11)] = 525
    edgW[str(11)+" "+str(49)] = 181
    edgW[str(49)+" "+str(11)] = 181
    edgW[str(12)+" "+str(15)] = 322
    edgW[str(15)+" "+str(12)] = 322
    edgW[str(12)+" "+str(20)] = 493
    edgW[str(20)+" "+str(12)] = 493
    edgW[str(12)+" "+str(22)] = 808
    edgW[str(22)+" "+str(12)] = 808
    edgW[str(12)+" "+str(29)] = 179
    edgW[str(29)+" "+str(12)] = 179
    edgW[str(12)+" "+str(33)] = 687
    edgW[str(33)+" "+str(12)] = 687
    edgW[str(12)+" "+str(34)] = 861
    edgW[str(34)+" "+str(12)] = 861
    edgW[str(13)+" "+str(21)] = 537
    edgW[str(21)+" "+str(13)] = 537
    edgW[str(13)+" "+str(26)] = 307
    edgW[str(26)+" "+str(13)] = 307
    edgW[str(13)+" "+str(43)] = 862
    edgW[str(43)+" "+str(13)] = 862
    edgW[str(13)+" "+str(44)] = 529
    edgW[str(44)+" "+str(13)] = 529
    edgW[str(14)+" "+str(18)] = 144
    edgW[str(18)+" "+str(14)] = 144
    edgW[str(14)+" "+str(26)] = 757
    edgW[str(26)+" "+str(14)] = 757
    edgW[str(14)+" "+str(30)] = 474
    edgW[str(30)+" "+str(14)] = 474
    edgW[str(14)+" "+str(31)] = 529
    edgW[str(31)+" "+str(14)] = 529
    edgW[str(14)+" "+str(34)] = 630
    edgW[str(34)+" "+str(14)] = 630
    edgW[str(14)+" "+str(36)] = 247
    edgW[str(36)+" "+str(14)] = 247
    edgW[str(14)+" "+str(39)] = 616
    edgW[str(39)+" "+str(14)] = 616
    edgW[str(14)+" "+str(41)] = 403
    edgW[str(41)+" "+str(14)] = 403
    edgW[str(14)+" "+str(42)] = 117
    edgW[str(42)+" "+str(14)] = 117
    edgW[str(14)+" "+str(43)] = 342
    edgW[str(43)+" "+str(14)] = 342
    edgW[str(14)+" "+str(44)] = 69
    edgW[str(44)+" "+str(14)] = 69
    edgW[str(15)+" "+str(20)] = 450
    edgW[str(20)+" "+str(15)] = 450
    edgW[str(15)+" "+str(40)] = 944
    edgW[str(40)+" "+str(15)] = 944
    edgW[str(16)+" "+str(37)] = 237
    edgW[str(37)+" "+str(16)] = 237
    edgW[str(17)+" "+str(21)] = 145
    edgW[str(21)+" "+str(17)] = 145
    edgW[str(17)+" "+str(25)] = 606
    edgW[str(25)+" "+str(17)] = 606
    edgW[str(17)+" "+str(43)] = 904
    edgW[str(43)+" "+str(17)] = 904
    edgW[str(17)+" "+str(44)] = 981
    edgW[str(44)+" "+str(17)] = 981
    edgW[str(17)+" "+str(45)] = 385
    edgW[str(45)+" "+str(17)] = 385
    edgW[str(18)+" "+str(24)] = 215
    edgW[str(24)+" "+str(18)] = 215
    edgW[str(18)+" "+str(28)] = 944
    edgW[str(28)+" "+str(18)] = 944
    edgW[str(18)+" "+str(29)] = 314
    edgW[str(29)+" "+str(18)] = 314
    edgW[str(18)+" "+str(50)] = 272
    edgW[str(50)+" "+str(18)] = 272
    edgW[str(19)+" "+str(21)] = 824
    edgW[str(21)+" "+str(19)] = 824
    edgW[str(19)+" "+str(45)] = 328
    edgW[str(45)+" "+str(19)] = 328
    edgW[str(19)+" "+str(47)] = 466
    edgW[str(47)+" "+str(19)] = 466
    edgW[str(20)+" "+str(27)] = 299
    edgW[str(27)+" "+str(20)] = 299
    edgW[str(20)+" "+str(29)] = 725
    edgW[str(29)+" "+str(20)] = 725
    edgW[str(20)+" "+str(49)] = 575
    edgW[str(49)+" "+str(20)] = 575
    edgW[str(20)+" "+str(50)] = 696
    edgW[str(50)+" "+str(20)] = 696
    edgW[str(21)+" "+str(37)] = 961
    edgW[str(37)+" "+str(21)] = 961
    edgW[str(21)+" "+str(40)] = 877
    edgW[str(40)+" "+str(21)] = 877
    edgW[str(21)+" "+str(42)] = 87
    edgW[str(42)+" "+str(21)] = 87
    edgW[str(21)+" "+str(47)] = 646
    edgW[str(47)+" "+str(21)] = 646
    edgW[str(21)+" "+str(48)] = 993
    edgW[str(48)+" "+str(21)] = 993
    edgW[str(21)+" "+str(50)] = 975
    edgW[str(50)+" "+str(21)] = 975
    edgW[str(22)+" "+str(28)] = 744
    edgW[str(28)+" "+str(22)] = 744
    edgW[str(22)+" "+str(31)] = 160
    edgW[str(31)+" "+str(22)] = 160
    edgW[str(22)+" "+str(33)] = 889
    edgW[str(33)+" "+str(22)] = 889
    edgW[str(22)+" "+str(39)] = 469
    edgW[str(39)+" "+str(22)] = 469
    edgW[str(22)+" "+str(40)] = 200
    edgW[str(40)+" "+str(22)] = 200
    edgW[str(22)+" "+str(47)] = 76
    edgW[str(47)+" "+str(22)] = 76
    edgW[str(23)+" "+str(30)] = 16
    edgW[str(30)+" "+str(23)] = 16
    edgW[str(23)+" "+str(31)] = 28
    edgW[str(31)+" "+str(23)] = 28
    edgW[str(23)+" "+str(34)] = 95
    edgW[str(34)+" "+str(23)] = 95
    edgW[str(23)+" "+str(41)] = 481
    edgW[str(41)+" "+str(23)] = 481
    edgW[str(23)+" "+str(44)] = 21
    edgW[str(44)+" "+str(23)] = 21
    edgW[str(23)+" "+str(45)] = 44
    edgW[str(45)+" "+str(23)] = 44
    edgW[str(23)+" "+str(48)] = 593
    edgW[str(48)+" "+str(23)] = 593
    edgW[str(23)+" "+str(50)] = 709
    edgW[str(50)+" "+str(23)] = 709
    edgW[str(24)+" "+str(25)] = 339
    edgW[str(25)+" "+str(24)] = 339
    edgW[str(24)+" "+str(26)] = 39
    edgW[str(26)+" "+str(24)] = 39
    edgW[str(24)+" "+str(33)] = 581
    edgW[str(33)+" "+str(24)] = 581
    edgW[str(24)+" "+str(36)] = 556
    edgW[str(36)+" "+str(24)] = 556
    edgW[str(24)+" "+str(43)] = 356
    edgW[str(43)+" "+str(24)] = 356
    edgW[str(24)+" "+str(48)] = 907
    edgW[str(48)+" "+str(24)] = 907
    edgW[str(25)+" "+str(26)] = 526
    edgW[str(26)+" "+str(25)] = 526
    edgW[str(25)+" "+str(27)] = 183
    edgW[str(27)+" "+str(25)] = 183
    edgW[str(25)+" "+str(28)] = 822
    edgW[str(28)+" "+str(25)] = 822
    edgW[str(25)+" "+str(35)] = 316
    edgW[str(35)+" "+str(25)] = 316
    edgW[str(25)+" "+str(42)] = 526
    edgW[str(42)+" "+str(25)] = 526
    edgW[str(25)+" "+str(47)] = 165
    edgW[str(47)+" "+str(25)] = 165
    edgW[str(25)+" "+str(49)] = 30
    edgW[str(49)+" "+str(25)] = 30
    edgW[str(26)+" "+str(41)] = 694
    edgW[str(41)+" "+str(26)] = 694
    edgW[str(26)+" "+str(43)] = 86
    edgW[str(43)+" "+str(26)] = 86
    edgW[str(27)+" "+str(30)] = 469
    edgW[str(30)+" "+str(27)] = 469
    edgW[str(27)+" "+str(32)] = 827
    edgW[str(32)+" "+str(27)] = 827
    edgW[str(27)+" "+str(33)] = 67
    edgW[str(33)+" "+str(27)] = 67
    edgW[str(27)+" "+str(35)] = 9
    edgW[str(35)+" "+str(27)] = 9
    edgW[str(27)+" "+str(47)] = 954
    edgW[str(47)+" "+str(27)] = 954
    edgW[str(28)+" "+str(37)] = 481
    edgW[str(37)+" "+str(28)] = 481
    edgW[str(28)+" "+str(40)] = 214
    edgW[str(40)+" "+str(28)] = 214
    edgW[str(28)+" "+str(42)] = 544
    edgW[str(42)+" "+str(28)] = 544
    edgW[str(28)+" "+str(49)] = 847
    edgW[str(49)+" "+str(28)] = 847
    edgW[str(29)+" "+str(30)] = 190
    edgW[str(30)+" "+str(29)] = 190
    edgW[str(29)+" "+str(31)] = 294
    edgW[str(31)+" "+str(29)] = 294
    edgW[str(29)+" "+str(36)] = 433
    edgW[str(36)+" "+str(29)] = 433
    edgW[str(30)+" "+str(42)] = 728
    edgW[str(42)+" "+str(30)] = 728
    edgW[str(30)+" "+str(49)] = 962
    edgW[str(49)+" "+str(30)] = 962
    edgW[str(31)+" "+str(32)] = 175
    edgW[str(32)+" "+str(31)] = 175
    edgW[str(31)+" "+str(33)] = 920
    edgW[str(33)+" "+str(31)] = 920
    edgW[str(31)+" "+str(45)] = 851
    edgW[str(45)+" "+str(31)] = 851
    edgW[str(31)+" "+str(46)] = 363
    edgW[str(46)+" "+str(31)] = 363
    edgW[str(32)+" "+str(37)] = 411
    edgW[str(37)+" "+str(32)] = 411
    edgW[str(32)+" "+str(46)] = 722
    edgW[str(46)+" "+str(32)] = 722
    edgW[str(33)+" "+str(38)] = 381
    edgW[str(38)+" "+str(33)] = 381
    edgW[str(33)+" "+str(45)] = 39
    edgW[str(45)+" "+str(33)] = 39
    edgW[str(34)+" "+str(40)] = 557
    edgW[str(40)+" "+str(34)] = 557
    edgW[str(34)+" "+str(41)] = 283
    edgW[str(41)+" "+str(34)] = 283
    edgW[str(34)+" "+str(49)] = 267
    edgW[str(49)+" "+str(34)] = 267
    edgW[str(35)+" "+str(38)] = 345
    edgW[str(38)+" "+str(35)] = 345
    edgW[str(35)+" "+str(39)] = 112
    edgW[str(39)+" "+str(35)] = 112
    edgW[str(35)+" "+str(42)] = 398
    edgW[str(42)+" "+str(35)] = 398
    edgW[str(36)+" "+str(39)] = 978
    edgW[str(39)+" "+str(36)] = 978
    edgW[str(36)+" "+str(43)] = 911
    edgW[str(43)+" "+str(36)] = 911
    edgW[str(37)+" "+str(39)] = 358
    edgW[str(39)+" "+str(37)] = 358
    edgW[str(37)+" "+str(41)] = 284
    edgW[str(41)+" "+str(37)] = 284
    edgW[str(37)+" "+str(46)] = 131
    edgW[str(46)+" "+str(37)] = 131
    edgW[str(37)+" "+str(49)] = 945
    edgW[str(49)+" "+str(37)] = 945
    edgW[str(37)+" "+str(50)] = 461
    edgW[str(50)+" "+str(37)] = 461
    edgW[str(38)+" "+str(39)] = 885
    edgW[str(39)+" "+str(38)] = 885
    edgW[str(39)+" "+str(42)] = 286
    edgW[str(42)+" "+str(39)] = 286
    edgW[str(39)+" "+str(43)] = 6
    edgW[str(43)+" "+str(39)] = 6
    edgW[str(39)+" "+str(47)] = 961
    edgW[str(47)+" "+str(39)] = 961
    edgW[str(41)+" "+str(44)] = 750
    edgW[str(44)+" "+str(41)] = 750
    edgW[str(41)+" "+str(47)] = 550
    edgW[str(47)+" "+str(41)] = 550
    edgW[str(43)+" "+str(44)] = 302
    edgW[str(44)+" "+str(43)] = 302
    edgW[str(45)+" "+str(47)] = 74
    edgW[str(47)+" "+str(45)] = 74
    edgW[str(45)+" "+str(48)] = 197
    edgW[str(48)+" "+str(45)] = 197
    edgW[str(46)+" "+str(48)] = 842
    edgW[str(48)+" "+str(46)] = 842

    return edgW

def getGraph():
    GT = [[],
            [2, 3, 4, 5, 6, 7, 11, 14, 22, 28, 38, 39, 46, 49],
            [1, 11, 15, 16, 18, 19, 37, 38],
            [1, 16, 18, 22, 30, 31, 32],
            [1, 5, 6, 8, 21, 34, 38],
            [1, 4, 14, 25, 30, 35, 38, 41],
            [1, 4, 7, 10, 25, 28, 35, 38, 39, 41],
            [1, 6, 13, 17, 19, 20, 26, 46, 47, 49],
            [4, 9, 22, 24, 42, 43],
            [8, 14, 16, 23, 33, 36, 37, 42],
            [6, 12, 21, 22, 35, 36, 50],
            [1, 2, 19, 24, 33, 40, 45, 47, 49],
            [10, 15, 20, 22, 29, 33, 34],
            [7, 21, 26, 43, 44],
            [1, 5, 9, 18, 26, 30, 31, 34, 36, 39, 41, 42, 43, 44],
            [2, 12, 20, 40],
            [2, 3, 9, 37],
            [7, 21, 25, 43, 44, 45],
            [2, 3, 14, 24, 28, 29, 50],
            [2, 7, 11, 21, 45, 47],
            [7, 12, 15, 27, 29, 49, 50],
            [4, 10, 13, 17, 19, 37, 40, 42, 47, 48, 50],
            [1, 3, 8, 10, 12, 28, 31, 33, 39, 40, 47],
            [9, 30, 31, 34, 41, 44, 45, 48, 50],
            [8, 11, 18, 25, 26, 33, 36, 43, 48],
            [5, 6, 17, 24, 26, 27, 28, 35, 42, 47, 49],
            [7, 13, 14, 24, 25, 41, 43],
            [20, 25, 30, 32, 33, 35, 47],
            [1, 6, 18, 22, 25, 37, 40, 42, 49],
            [12, 18, 20, 30, 31, 36],
            [3, 5, 14, 23, 27, 29, 42, 49],
            [3, 14, 22, 23, 29, 32, 33, 45, 46],
            [3, 27, 31, 37, 46],
            [9, 11, 12, 22, 24, 27, 31, 38, 45],
            [4, 12, 14, 23, 40, 41, 49],
            [5, 6, 10, 25, 27, 38, 39, 42],
            [9, 10, 14, 24, 29, 39, 43],
            [2, 9, 16, 21, 28, 32, 39, 41, 46, 49, 50],
            [1, 2, 4, 5, 6, 33, 35, 39],
            [1, 6, 14, 22, 35, 36, 37, 38, 42, 43, 47],
            [11, 15, 21, 22, 28, 34],
            [5, 6, 14, 23, 26, 34, 37, 44, 47],
            [8, 9, 14, 21, 25, 28, 30, 35, 39],
            [8, 13, 14, 17, 24, 26, 36, 39, 44],
            [13, 14, 17, 23, 41, 43],
            [11, 17, 19, 23, 31, 33, 47, 48],
            [1, 7, 31, 32, 37, 48],
            [7, 11, 19, 21, 22, 25, 27, 39, 41, 45],
            [21, 23, 24, 45, 46],
            [1, 7, 11, 20, 25, 28, 30, 34, 37],
            [10, 18, 20, 21, 23, 37]
        ]
    return GT

def getWeights1():
    edgW = {}
    edgW[str(1)+" "+str(2)] = 155
    edgW[str(2)+" "+str(1)] = 155
    edgW[str(1)+" "+str(3)] = 751
    edgW[str(3)+" "+str(1)] = 751
    edgW[str(1)+" "+str(4)] = 726
    edgW[str(4)+" "+str(1)] = 726
    edgW[str(1)+" "+str(5)] = 89
    edgW[str(5)+" "+str(1)] = 89
    edgW[str(1)+" "+str(7)] = 63
    edgW[str(7)+" "+str(1)] = 63
    edgW[str(1)+" "+str(9)] = 700
    edgW[str(9)+" "+str(1)] = 700
    edgW[str(1)+" "+str(10)] = 336
    edgW[str(10)+" "+str(1)] = 336
    edgW[str(2)+" "+str(3)] = 180
    edgW[str(3)+" "+str(2)] = 180
    edgW[str(2)+" "+str(4)] = 66
    edgW[str(4)+" "+str(2)] = 66
    edgW[str(2)+" "+str(5)] = 409
    edgW[str(5)+" "+str(2)] = 409
    edgW[str(2)+" "+str(6)] = 579
    edgW[str(6)+" "+str(2)] = 579
    edgW[str(2)+" "+str(7)] = 305
    edgW[str(7)+" "+str(2)] = 305
    edgW[str(2)+" "+str(8)] = 555
    edgW[str(8)+" "+str(2)] = 555
    edgW[str(2)+" "+str(10)] = 442
    edgW[str(10)+" "+str(2)] = 442
    edgW[str(3)+" "+str(10)] = 799
    edgW[str(10)+" "+str(3)] = 799
    edgW[str(3)+" "+str(4)] = 889
    edgW[str(4)+" "+str(3)] = 889
    edgW[str(3)+" "+str(6)] = 714
    edgW[str(6)+" "+str(3)] = 714
    edgW[str(3)+" "+str(7)] = 414
    edgW[str(7)+" "+str(3)] = 414
    edgW[str(3)+" "+str(8)] = 252
    edgW[str(8)+" "+str(3)] = 252
    edgW[str(4)+" "+str(6)] = 772
    edgW[str(6)+" "+str(4)] = 772
    edgW[str(4)+" "+str(9)] = 517
    edgW[str(9)+" "+str(4)] = 517
    edgW[str(5)+" "+str(7)] = 789
    edgW[str(7)+" "+str(5)] = 789
    edgW[str(5)+" "+str(9)] = 323
    edgW[str(9)+" "+str(5)] = 323
    edgW[str(6)+" "+str(10)] = 472
    edgW[str(10)+" "+str(6)] = 472
    edgW[str(6)+" "+str(7)] = 374
    edgW[str(7)+" "+str(6)] = 374
    edgW[str(6)+" "+str(8)] = 923
    edgW[str(8)+" "+str(6)] = 923
    edgW[str(6)+" "+str(9)] = 502
    edgW[str(9)+" "+str(6)] = 502
    edgW[str(7)+" "+str(8)] = 705
    edgW[str(8)+" "+str(7)] = 705
    edgW[str(8)+" "+str(10)] = 730
    edgW[str(10)+" "+str(8)] = 730
    edgW[str(8)+" "+str(9)] = 524
    edgW[str(9)+" "+str(8)] = 524
    return edgW

def getGraph1():
    GT = [[],
     [ 2, 3, 4, 5, 7, 9, 10], 
     [ 1, 3, 4, 6, 7, 8, 10],
     [ 10, 1, 4, 2, 6, 7, 8],
     [ 2, 1, 3, 6, 9],
     [ 1, 2, 7, 9],
     [ 10, 2, 3, 4, 7, 8, 9],
     [ 1, 6, 5, 3, 2, 8],
     [ 10, 2, 3, 6, 7, 9],
     [ 1, 4, 5, 6, 8],
     [ 3, 6, 8, 2, 1]]

    return GT

def getWeights3():
    edgW = {}

    edgW[str(1)+" "+str(2)] = 373
    edgW[str(2)+" "+str(1)] = 373
    edgW[str(1)+" "+str(44)] = 848
    edgW[str(44)+" "+str(1)] = 848
    edgW[str(1)+" "+str(95)] = 113
    edgW[str(95)+" "+str(1)] = 113
    edgW[str(1)+" "+str(102)] = 755
    edgW[str(102)+" "+str(1)] = 755
    edgW[str(1)+" "+str(162)] = 37
    edgW[str(162)+" "+str(1)] = 37
    edgW[str(1)+" "+str(166)] = 259
    edgW[str(166)+" "+str(1)] = 259
    edgW[str(2)+" "+str(3)] = 83
    edgW[str(3)+" "+str(2)] = 83
    edgW[str(2)+" "+str(6)] = 99
    edgW[str(6)+" "+str(2)] = 99
    edgW[str(2)+" "+str(119)] = 654
    edgW[str(119)+" "+str(2)] = 654
    edgW[str(2)+" "+str(156)] = 105
    edgW[str(156)+" "+str(2)] = 105
    edgW[str(3)+" "+str(4)] = 711
    edgW[str(4)+" "+str(3)] = 711
    edgW[str(3)+" "+str(8)] = 774
    edgW[str(8)+" "+str(3)] = 774
    edgW[str(3)+" "+str(26)] = 160
    edgW[str(26)+" "+str(3)] = 160
    edgW[str(3)+" "+str(57)] = 482
    edgW[str(57)+" "+str(3)] = 482
    edgW[str(3)+" "+str(65)] = 458
    edgW[str(65)+" "+str(3)] = 458
    edgW[str(3)+" "+str(69)] = 544
    edgW[str(69)+" "+str(3)] = 544
    edgW[str(3)+" "+str(108)] = 261
    edgW[str(108)+" "+str(3)] = 261
    edgW[str(3)+" "+str(123)] = 782
    edgW[str(123)+" "+str(3)] = 782
    edgW[str(3)+" "+str(170)] = 695
    edgW[str(170)+" "+str(3)] = 695
    edgW[str(4)+" "+str(5)] = 362
    edgW[str(5)+" "+str(4)] = 362
    edgW[str(4)+" "+str(7)] = 20
    edgW[str(7)+" "+str(4)] = 20
    edgW[str(4)+" "+str(12)] = 734
    edgW[str(12)+" "+str(4)] = 734
    edgW[str(4)+" "+str(21)] = 696
    edgW[str(21)+" "+str(4)] = 696
    edgW[str(4)+" "+str(68)] = 193
    edgW[str(68)+" "+str(4)] = 193
    edgW[str(4)+" "+str(74)] = 905
    edgW[str(74)+" "+str(4)] = 905
    edgW[str(4)+" "+str(198)] = 80
    edgW[str(198)+" "+str(4)] = 80
    edgW[str(5)+" "+str(10)] = 827
    edgW[str(10)+" "+str(5)] = 827
    edgW[str(5)+" "+str(18)] = 319
    edgW[str(18)+" "+str(5)] = 319
    edgW[str(5)+" "+str(23)] = 991
    edgW[str(23)+" "+str(5)] = 991
    edgW[str(5)+" "+str(55)] = 230
    edgW[str(55)+" "+str(5)] = 230
    edgW[str(5)+" "+str(81)] = 409
    edgW[str(81)+" "+str(5)] = 409
    edgW[str(5)+" "+str(175)] = 364
    edgW[str(175)+" "+str(5)] = 364
    edgW[str(6)+" "+str(17)] = 538
    edgW[str(17)+" "+str(6)] = 538
    edgW[str(6)+" "+str(46)] = 336
    edgW[str(46)+" "+str(6)] = 336
    edgW[str(6)+" "+str(52)] = 189
    edgW[str(52)+" "+str(6)] = 189
    edgW[str(6)+" "+str(71)] = 551
    edgW[str(71)+" "+str(6)] = 551
    edgW[str(6)+" "+str(92)] = 15
    edgW[str(92)+" "+str(6)] = 15
    edgW[str(7)+" "+str(15)] = 8
    edgW[str(15)+" "+str(7)] = 8
    edgW[str(7)+" "+str(19)] = 329
    edgW[str(19)+" "+str(7)] = 329
    edgW[str(7)+" "+str(25)] = 877
    edgW[str(25)+" "+str(7)] = 877
    edgW[str(7)+" "+str(85)] = 100
    edgW[str(85)+" "+str(7)] = 100
    edgW[str(7)+" "+str(100)] = 742
    edgW[str(100)+" "+str(7)] = 742
    edgW[str(7)+" "+str(130)] = 838
    edgW[str(130)+" "+str(7)] = 838
    edgW[str(7)+" "+str(195)] = 967
    edgW[str(195)+" "+str(7)] = 967
    edgW[str(8)+" "+str(9)] = 860
    edgW[str(9)+" "+str(8)] = 860
    edgW[str(8)+" "+str(11)] = 978
    edgW[str(11)+" "+str(8)] = 978
    edgW[str(8)+" "+str(13)] = 16
    edgW[str(13)+" "+str(8)] = 16
    edgW[str(8)+" "+str(16)] = 964
    edgW[str(16)+" "+str(8)] = 964
    edgW[str(8)+" "+str(38)] = 435
    edgW[str(38)+" "+str(8)] = 435
    edgW[str(8)+" "+str(40)] = 809
    edgW[str(40)+" "+str(8)] = 809
    edgW[str(8)+" "+str(115)] = 690
    edgW[str(115)+" "+str(8)] = 690
    edgW[str(8)+" "+str(131)] = 393
    edgW[str(131)+" "+str(8)] = 393
    edgW[str(9)+" "+str(14)] = 283
    edgW[str(14)+" "+str(9)] = 283
    edgW[str(9)+" "+str(34)] = 758
    edgW[str(34)+" "+str(9)] = 758
    edgW[str(9)+" "+str(42)] = 62
    edgW[str(42)+" "+str(9)] = 62
    edgW[str(9)+" "+str(51)] = 316
    edgW[str(51)+" "+str(9)] = 316
    edgW[str(9)+" "+str(191)] = 328
    edgW[str(191)+" "+str(9)] = 328
    edgW[str(10)+" "+str(19)] = 499
    edgW[str(19)+" "+str(10)] = 499
    edgW[str(10)+" "+str(30)] = 452
    edgW[str(30)+" "+str(10)] = 452
    edgW[str(10)+" "+str(41)] = 710
    edgW[str(41)+" "+str(10)] = 710
    edgW[str(10)+" "+str(86)] = 387
    edgW[str(86)+" "+str(10)] = 387
    edgW[str(10)+" "+str(122)] = 629
    edgW[str(122)+" "+str(10)] = 629
    edgW[str(11)+" "+str(56)] = 995
    edgW[str(56)+" "+str(11)] = 995
    edgW[str(11)+" "+str(98)] = 347
    edgW[str(98)+" "+str(11)] = 347
    edgW[str(11)+" "+str(99)] = 581
    edgW[str(99)+" "+str(11)] = 581
    edgW[str(11)+" "+str(117)] = 586
    edgW[str(117)+" "+str(11)] = 586
    edgW[str(11)+" "+str(129)] = 541
    edgW[str(129)+" "+str(11)] = 541
    edgW[str(11)+" "+str(132)] = 834
    edgW[str(132)+" "+str(11)] = 834
    edgW[str(11)+" "+str(176)] = 27
    edgW[str(176)+" "+str(11)] = 27
    edgW[str(12)+" "+str(35)] = 29
    edgW[str(35)+" "+str(12)] = 29
    edgW[str(12)+" "+str(49)] = 62
    edgW[str(49)+" "+str(12)] = 62
    edgW[str(12)+" "+str(139)] = 497
    edgW[str(139)+" "+str(12)] = 497
    edgW[str(12)+" "+str(161)] = 109
    edgW[str(161)+" "+str(12)] = 109
    edgW[str(13)+" "+str(27)] = 585
    edgW[str(27)+" "+str(13)] = 585
    edgW[str(13)+" "+str(51)] = 940
    edgW[str(51)+" "+str(13)] = 940
    edgW[str(13)+" "+str(83)] = 388
    edgW[str(83)+" "+str(13)] = 388
    edgW[str(13)+" "+str(185)] = 161
    edgW[str(185)+" "+str(13)] = 161
    edgW[str(14)+" "+str(145)] = 389
    edgW[str(145)+" "+str(14)] = 389
    edgW[str(15)+" "+str(27)] = 656
    edgW[str(27)+" "+str(15)] = 656
    edgW[str(15)+" "+str(63)] = 667
    edgW[str(63)+" "+str(15)] = 667
    edgW[str(15)+" "+str(79)] = 272
    edgW[str(79)+" "+str(15)] = 272
    edgW[str(15)+" "+str(148)] = 539
    edgW[str(148)+" "+str(15)] = 539
    edgW[str(15)+" "+str(161)] = 33
    edgW[str(161)+" "+str(15)] = 33
    edgW[str(16)+" "+str(108)] = 693
    edgW[str(108)+" "+str(16)] = 693
    edgW[str(17)+" "+str(27)] = 488
    edgW[str(27)+" "+str(17)] = 488
    edgW[str(17)+" "+str(71)] = 824
    edgW[str(71)+" "+str(17)] = 824
    edgW[str(17)+" "+str(73)] = 103
    edgW[str(73)+" "+str(17)] = 103
    edgW[str(17)+" "+str(109)] = 914
    edgW[str(109)+" "+str(17)] = 914
    edgW[str(17)+" "+str(117)] = 986
    edgW[str(117)+" "+str(17)] = 986
    edgW[str(17)+" "+str(149)] = 207
    edgW[str(149)+" "+str(17)] = 207
    edgW[str(17)+" "+str(190)] = 950
    edgW[str(190)+" "+str(17)] = 950
    edgW[str(18)+" "+str(20)] = 129
    edgW[str(20)+" "+str(18)] = 129
    edgW[str(18)+" "+str(29)] = 308
    edgW[str(29)+" "+str(18)] = 308
    edgW[str(18)+" "+str(128)] = 765
    edgW[str(128)+" "+str(18)] = 765
    edgW[str(19)+" "+str(32)] = 497
    edgW[str(32)+" "+str(19)] = 497
    edgW[str(19)+" "+str(37)] = 980
    edgW[str(37)+" "+str(19)] = 980
    edgW[str(19)+" "+str(53)] = 934
    edgW[str(53)+" "+str(19)] = 934
    edgW[str(19)+" "+str(54)] = 366
    edgW[str(54)+" "+str(19)] = 366
    edgW[str(19)+" "+str(197)] = 233
    edgW[str(197)+" "+str(19)] = 233
    edgW[str(20)+" "+str(22)] = 660
    edgW[str(22)+" "+str(20)] = 660
    edgW[str(20)+" "+str(24)] = 429
    edgW[str(24)+" "+str(20)] = 429
    edgW[str(20)+" "+str(116)] = 518
    edgW[str(116)+" "+str(20)] = 518
    edgW[str(20)+" "+str(134)] = 301
    edgW[str(134)+" "+str(20)] = 301
    edgW[str(21)+" "+str(39)] = 732
    edgW[str(39)+" "+str(21)] = 732
    edgW[str(21)+" "+str(91)] = 971
    edgW[str(91)+" "+str(21)] = 971
    edgW[str(21)+" "+str(126)] = 920
    edgW[str(126)+" "+str(21)] = 920
    edgW[str(21)+" "+str(142)] = 664
    edgW[str(142)+" "+str(21)] = 664
    edgW[str(21)+" "+str(173)] = 422
    edgW[str(173)+" "+str(21)] = 422
    edgW[str(22)+" "+str(42)] = 818
    edgW[str(42)+" "+str(22)] = 818
    edgW[str(22)+" "+str(61)] = 886
    edgW[str(61)+" "+str(22)] = 886
    edgW[str(22)+" "+str(147)] = 786
    edgW[str(147)+" "+str(22)] = 786
    edgW[str(22)+" "+str(152)] = 921
    edgW[str(152)+" "+str(22)] = 921
    edgW[str(22)+" "+str(177)] = 658
    edgW[str(177)+" "+str(22)] = 658
    edgW[str(23)+" "+str(33)] = 307
    edgW[str(33)+" "+str(23)] = 307
    edgW[str(23)+" "+str(111)] = 4
    edgW[str(111)+" "+str(23)] = 4
    edgW[str(23)+" "+str(150)] = 444
    edgW[str(150)+" "+str(23)] = 444
    edgW[str(24)+" "+str(31)] = 547
    edgW[str(31)+" "+str(24)] = 547
    edgW[str(24)+" "+str(54)] = 248
    edgW[str(54)+" "+str(24)] = 248
    edgW[str(24)+" "+str(82)] = 341
    edgW[str(82)+" "+str(24)] = 341
    edgW[str(24)+" "+str(127)] = 330
    edgW[str(127)+" "+str(24)] = 330
    edgW[str(24)+" "+str(187)] = 138
    edgW[str(187)+" "+str(24)] = 138
    edgW[str(25)+" "+str(28)] = 1
    edgW[str(28)+" "+str(25)] = 1
    edgW[str(25)+" "+str(48)] = 554
    edgW[str(48)+" "+str(25)] = 554
    edgW[str(25)+" "+str(73)] = 108
    edgW[str(73)+" "+str(25)] = 108
    edgW[str(26)+" "+str(48)] = 958
    edgW[str(48)+" "+str(26)] = 958
    edgW[str(26)+" "+str(179)] = 561
    edgW[str(179)+" "+str(26)] = 561
    edgW[str(26)+" "+str(196)] = 742
    edgW[str(196)+" "+str(26)] = 742
    edgW[str(27)+" "+str(30)] = 739
    edgW[str(30)+" "+str(27)] = 739
    edgW[str(27)+" "+str(44)] = 851
    edgW[str(44)+" "+str(27)] = 851
    edgW[str(28)+" "+str(83)] = 915
    edgW[str(83)+" "+str(28)] = 915
    edgW[str(28)+" "+str(93)] = 725
    edgW[str(93)+" "+str(28)] = 725
    edgW[str(28)+" "+str(118)] = 778
    edgW[str(118)+" "+str(28)] = 778
    edgW[str(28)+" "+str(138)] = 181
    edgW[str(138)+" "+str(28)] = 181
    edgW[str(29)+" "+str(45)] = 140
    edgW[str(45)+" "+str(29)] = 140
    edgW[str(29)+" "+str(53)] = 740
    edgW[str(53)+" "+str(29)] = 740
    edgW[str(29)+" "+str(95)] = 74
    edgW[str(95)+" "+str(29)] = 74
    edgW[str(29)+" "+str(124)] = 234
    edgW[str(124)+" "+str(29)] = 234
    edgW[str(29)+" "+str(159)] = 552
    edgW[str(159)+" "+str(29)] = 552
    edgW[str(30)+" "+str(31)] = 720
    edgW[str(31)+" "+str(30)] = 720
    edgW[str(30)+" "+str(64)] = 367
    edgW[str(64)+" "+str(30)] = 367
    edgW[str(30)+" "+str(77)] = 712
    edgW[str(77)+" "+str(30)] = 712
    edgW[str(30)+" "+str(133)] = 225
    edgW[str(133)+" "+str(30)] = 225
    edgW[str(30)+" "+str(163)] = 730
    edgW[str(163)+" "+str(30)] = 730
    edgW[str(30)+" "+str(174)] = 935
    edgW[str(174)+" "+str(30)] = 935
    edgW[str(30)+" "+str(177)] = 368
    edgW[str(177)+" "+str(30)] = 368
    edgW[str(31)+" "+str(109)] = 458
    edgW[str(109)+" "+str(31)] = 458
    edgW[str(31)+" "+str(110)] = 357
    edgW[str(110)+" "+str(31)] = 357
    edgW[str(31)+" "+str(116)] = 293
    edgW[str(116)+" "+str(31)] = 293
    edgW[str(32)+" "+str(113)] = 509
    edgW[str(113)+" "+str(32)] = 509
    edgW[str(33)+" "+str(125)] = 567
    edgW[str(125)+" "+str(33)] = 567
    edgW[str(33)+" "+str(126)] = 234
    edgW[str(126)+" "+str(33)] = 234
    edgW[str(34)+" "+str(47)] = 478
    edgW[str(47)+" "+str(34)] = 478
    edgW[str(34)+" "+str(48)] = 880
    edgW[str(48)+" "+str(34)] = 880
    edgW[str(34)+" "+str(51)] = 297
    edgW[str(51)+" "+str(34)] = 297
    edgW[str(34)+" "+str(78)] = 57
    edgW[str(78)+" "+str(34)] = 57
    edgW[str(34)+" "+str(93)] = 122
    edgW[str(93)+" "+str(34)] = 122
    edgW[str(34)+" "+str(155)] = 919
    edgW[str(155)+" "+str(34)] = 919
    edgW[str(35)+" "+str(36)] = 186
    edgW[str(36)+" "+str(35)] = 186
    edgW[str(35)+" "+str(58)] = 140
    edgW[str(58)+" "+str(35)] = 140
    edgW[str(35)+" "+str(89)] = 414
    edgW[str(89)+" "+str(35)] = 414
    edgW[str(35)+" "+str(90)] = 622
    edgW[str(90)+" "+str(35)] = 622
    edgW[str(35)+" "+str(93)] = 471
    edgW[str(93)+" "+str(35)] = 471
    edgW[str(35)+" "+str(139)] = 863
    edgW[str(139)+" "+str(35)] = 863
    edgW[str(35)+" "+str(156)] = 986
    edgW[str(156)+" "+str(35)] = 986
    edgW[str(35)+" "+str(172)] = 608
    edgW[str(172)+" "+str(35)] = 608
    edgW[str(35)+" "+str(173)] = 73
    edgW[str(173)+" "+str(35)] = 73
    edgW[str(37)+" "+str(43)] = 15
    edgW[str(43)+" "+str(37)] = 15
    edgW[str(37)+" "+str(126)] = 318
    edgW[str(126)+" "+str(37)] = 318
    edgW[str(37)+" "+str(144)] = 981
    edgW[str(144)+" "+str(37)] = 981
    edgW[str(38)+" "+str(55)] = 357
    edgW[str(55)+" "+str(38)] = 357
    edgW[str(39)+" "+str(59)] = 839
    edgW[str(59)+" "+str(39)] = 839
    edgW[str(39)+" "+str(119)] = 895
    edgW[str(119)+" "+str(39)] = 895
    edgW[str(39)+" "+str(141)] = 297
    edgW[str(141)+" "+str(39)] = 297
    edgW[str(39)+" "+str(191)] = 924
    edgW[str(191)+" "+str(39)] = 924
    edgW[str(40)+" "+str(62)] = 148
    edgW[str(62)+" "+str(40)] = 148
    edgW[str(40)+" "+str(100)] = 58
    edgW[str(100)+" "+str(40)] = 58
    edgW[str(40)+" "+str(120)] = 561
    edgW[str(120)+" "+str(40)] = 561
    edgW[str(40)+" "+str(157)] = 215
    edgW[str(157)+" "+str(40)] = 215
    edgW[str(41)+" "+str(66)] = 401
    edgW[str(66)+" "+str(41)] = 401
    edgW[str(42)+" "+str(60)] = 625
    edgW[str(60)+" "+str(42)] = 625
    edgW[str(42)+" "+str(76)] = 515
    edgW[str(76)+" "+str(42)] = 515
    edgW[str(42)+" "+str(104)] = 79
    edgW[str(104)+" "+str(42)] = 79
    edgW[str(42)+" "+str(142)] = 7
    edgW[str(142)+" "+str(42)] = 7
    edgW[str(42)+" "+str(149)] = 97
    edgW[str(149)+" "+str(42)] = 97
    edgW[str(42)+" "+str(162)] = 195
    edgW[str(162)+" "+str(42)] = 195
    edgW[str(42)+" "+str(181)] = 486
    edgW[str(181)+" "+str(42)] = 486
    edgW[str(43)+" "+str(46)] = 194
    edgW[str(46)+" "+str(43)] = 194
    edgW[str(43)+" "+str(96)] = 496
    edgW[str(96)+" "+str(43)] = 496
    edgW[str(43)+" "+str(124)] = 268
    edgW[str(124)+" "+str(43)] = 268
    edgW[str(43)+" "+str(196)] = 553
    edgW[str(196)+" "+str(43)] = 553
    edgW[str(44)+" "+str(64)] = 740
    edgW[str(64)+" "+str(44)] = 740
    edgW[str(46)+" "+str(106)] = 745
    edgW[str(106)+" "+str(46)] = 745
    edgW[str(46)+" "+str(112)] = 924
    edgW[str(112)+" "+str(46)] = 924
    edgW[str(46)+" "+str(182)] = 284
    edgW[str(182)+" "+str(46)] = 284
    edgW[str(47)+" "+str(50)] = 374
    edgW[str(50)+" "+str(47)] = 374
    edgW[str(47)+" "+str(78)] = 608
    edgW[str(78)+" "+str(47)] = 608
    edgW[str(47)+" "+str(156)] = 895
    edgW[str(156)+" "+str(47)] = 895
    edgW[str(47)+" "+str(198)] = 228
    edgW[str(198)+" "+str(47)] = 228
    edgW[str(49)+" "+str(57)] = 686
    edgW[str(57)+" "+str(49)] = 686
    edgW[str(49)+" "+str(99)] = 75
    edgW[str(99)+" "+str(49)] = 75
    edgW[str(49)+" "+str(154)] = 542
    edgW[str(154)+" "+str(49)] = 542
    edgW[str(49)+" "+str(176)] = 846
    edgW[str(176)+" "+str(49)] = 846
    edgW[str(49)+" "+str(199)] = 716
    edgW[str(199)+" "+str(49)] = 716
    edgW[str(50)+" "+str(81)] = 149
    edgW[str(81)+" "+str(50)] = 149
    edgW[str(50)+" "+str(88)] = 279
    edgW[str(88)+" "+str(50)] = 279
    edgW[str(50)+" "+str(99)] = 776
    edgW[str(99)+" "+str(50)] = 776
    edgW[str(50)+" "+str(126)] = 194
    edgW[str(126)+" "+str(50)] = 194
    edgW[str(50)+" "+str(157)] = 392
    edgW[str(157)+" "+str(50)] = 392
    edgW[str(50)+" "+str(169)] = 538
    edgW[str(169)+" "+str(50)] = 538
    edgW[str(51)+" "+str(55)] = 830
    edgW[str(55)+" "+str(51)] = 830
    edgW[str(51)+" "+str(70)] = 467
    edgW[str(70)+" "+str(51)] = 467
    edgW[str(51)+" "+str(155)] = 796
    edgW[str(155)+" "+str(51)] = 796
    edgW[str(52)+" "+str(67)] = 434
    edgW[str(67)+" "+str(52)] = 434
    edgW[str(52)+" "+str(72)] = 413
    edgW[str(72)+" "+str(52)] = 413
    edgW[str(52)+" "+str(155)] = 948
    edgW[str(155)+" "+str(52)] = 948
    edgW[str(52)+" "+str(172)] = 920
    edgW[str(172)+" "+str(52)] = 920
    edgW[str(53)+" "+str(102)] = 799
    edgW[str(102)+" "+str(53)] = 799
    edgW[str(54)+" "+str(97)] = 2
    edgW[str(97)+" "+str(54)] = 2
    edgW[str(54)+" "+str(138)] = 939
    edgW[str(138)+" "+str(54)] = 939
    edgW[str(55)+" "+str(115)] = 107
    edgW[str(115)+" "+str(55)] = 107
    edgW[str(55)+" "+str(136)] = 983
    edgW[str(136)+" "+str(55)] = 983
    edgW[str(55)+" "+str(200)] = 404
    edgW[str(200)+" "+str(55)] = 404
    edgW[str(58)+" "+str(59)] = 277
    edgW[str(59)+" "+str(58)] = 277
    edgW[str(58)+" "+str(157)] = 374
    edgW[str(157)+" "+str(58)] = 374
    edgW[str(59)+" "+str(135)] = 44
    edgW[str(135)+" "+str(59)] = 44
    edgW[str(60)+" "+str(136)] = 459
    edgW[str(136)+" "+str(60)] = 459
    edgW[str(60)+" "+str(194)] = 378
    edgW[str(194)+" "+str(60)] = 378
    edgW[str(60)+" "+str(200)] = 551
    edgW[str(200)+" "+str(60)] = 551
    edgW[str(61)+" "+str(169)] = 612
    edgW[str(169)+" "+str(61)] = 612
    edgW[str(62)+" "+str(110)] = 78
    edgW[str(110)+" "+str(62)] = 78
    edgW[str(62)+" "+str(180)] = 110
    edgW[str(180)+" "+str(62)] = 110
    edgW[str(63)+" "+str(87)] = 852
    edgW[str(87)+" "+str(63)] = 852
    edgW[str(63)+" "+str(114)] = 488
    edgW[str(114)+" "+str(63)] = 488
    edgW[str(63)+" "+str(140)] = 464
    edgW[str(140)+" "+str(63)] = 464
    edgW[str(63)+" "+str(184)] = 966
    edgW[str(184)+" "+str(63)] = 966
    edgW[str(64)+" "+str(151)] = 993
    edgW[str(151)+" "+str(64)] = 993
    edgW[str(64)+" "+str(165)] = 278
    edgW[str(165)+" "+str(64)] = 278
    edgW[str(65)+" "+str(84)] = 494
    edgW[str(84)+" "+str(65)] = 494
    edgW[str(65)+" "+str(95)] = 884
    edgW[str(95)+" "+str(65)] = 884
    edgW[str(65)+" "+str(188)] = 559
    edgW[str(188)+" "+str(65)] = 559
    edgW[str(66)+" "+str(75)] = 554
    edgW[str(75)+" "+str(66)] = 554
    edgW[str(66)+" "+str(149)] = 515
    edgW[str(149)+" "+str(66)] = 515
    edgW[str(66)+" "+str(152)] = 222
    edgW[str(152)+" "+str(66)] = 222
    edgW[str(67)+" "+str(180)] = 549
    edgW[str(180)+" "+str(67)] = 549
    edgW[str(68)+" "+str(74)] = 894
    edgW[str(74)+" "+str(68)] = 894
    edgW[str(68)+" "+str(83)] = 699
    edgW[str(83)+" "+str(68)] = 699
    edgW[str(68)+" "+str(157)] = 429
    edgW[str(157)+" "+str(68)] = 429
    edgW[str(68)+" "+str(164)] = 328
    edgW[str(164)+" "+str(68)] = 328
    edgW[str(69)+" "+str(121)] = 824
    edgW[str(121)+" "+str(69)] = 824
    edgW[str(69)+" "+str(122)] = 899
    edgW[str(122)+" "+str(69)] = 899
    edgW[str(69)+" "+str(137)] = 507
    edgW[str(137)+" "+str(69)] = 507
    edgW[str(71)+" "+str(84)] = 12
    edgW[str(84)+" "+str(71)] = 12
    edgW[str(71)+" "+str(109)] = 768
    edgW[str(109)+" "+str(71)] = 768
    edgW[str(71)+" "+str(148)] = 64
    edgW[str(148)+" "+str(71)] = 64
    edgW[str(71)+" "+str(166)] = 480
    edgW[str(166)+" "+str(71)] = 480
    edgW[str(72)+" "+str(116)] = 769
    edgW[str(116)+" "+str(72)] = 769
    edgW[str(72)+" "+str(197)] = 126
    edgW[str(197)+" "+str(72)] = 126
    edgW[str(72)+" "+str(198)] = 691
    edgW[str(198)+" "+str(72)] = 691
    edgW[str(73)+" "+str(75)] = 743
    edgW[str(75)+" "+str(73)] = 743
    edgW[str(73)+" "+str(89)] = 353
    edgW[str(89)+" "+str(73)] = 353
    edgW[str(74)+" "+str(141)] = 773
    edgW[str(141)+" "+str(74)] = 773
    edgW[str(75)+" "+str(107)] = 85
    edgW[str(107)+" "+str(75)] = 85
    edgW[str(75)+" "+str(118)] = 774
    edgW[str(118)+" "+str(75)] = 774
    edgW[str(76)+" "+str(78)] = 139
    edgW[str(78)+" "+str(76)] = 139
    edgW[str(76)+" "+str(81)] = 479
    edgW[str(81)+" "+str(76)] = 479
    edgW[str(76)+" "+str(139)] = 322
    edgW[str(139)+" "+str(76)] = 322
    edgW[str(76)+" "+str(162)] = 391
    edgW[str(162)+" "+str(76)] = 391
    edgW[str(76)+" "+str(167)] = 396
    edgW[str(167)+" "+str(76)] = 396
    edgW[str(77)+" "+str(141)] = 479
    edgW[str(141)+" "+str(77)] = 479
    edgW[str(77)+" "+str(157)] = 238
    edgW[str(157)+" "+str(77)] = 238
    edgW[str(77)+" "+str(165)] = 884
    edgW[str(165)+" "+str(77)] = 884
    edgW[str(77)+" "+str(179)] = 344
    edgW[str(179)+" "+str(77)] = 344
    edgW[str(77)+" "+str(186)] = 65
    edgW[str(186)+" "+str(77)] = 65
    edgW[str(78)+" "+str(121)] = 40
    edgW[str(121)+" "+str(78)] = 40
    edgW[str(79)+" "+str(80)] = 803
    edgW[str(80)+" "+str(79)] = 803
    edgW[str(79)+" "+str(158)] = 212
    edgW[str(158)+" "+str(79)] = 212
    edgW[str(80)+" "+str(154)] = 750
    edgW[str(154)+" "+str(80)] = 750
    edgW[str(81)+" "+str(161)] = 136
    edgW[str(161)+" "+str(81)] = 136
    edgW[str(82)+" "+str(101)] = 133
    edgW[str(101)+" "+str(82)] = 133
    edgW[str(83)+" "+str(139)] = 938
    edgW[str(139)+" "+str(83)] = 938
    edgW[str(84)+" "+str(110)] = 812
    edgW[str(110)+" "+str(84)] = 812
    edgW[str(85)+" "+str(94)] = 112
    edgW[str(94)+" "+str(85)] = 112
    edgW[str(85)+" "+str(97)] = 965
    edgW[str(97)+" "+str(85)] = 965
    edgW[str(85)+" "+str(182)] = 911
    edgW[str(182)+" "+str(85)] = 911
    edgW[str(86)+" "+str(126)] = 311
    edgW[str(126)+" "+str(86)] = 311
    edgW[str(86)+" "+str(127)] = 596
    edgW[str(127)+" "+str(86)] = 596
    edgW[str(86)+" "+str(134)] = 807
    edgW[str(134)+" "+str(86)] = 807
    edgW[str(86)+" "+str(146)] = 554
    edgW[str(146)+" "+str(86)] = 554
    edgW[str(87)+" "+str(181)] = 853
    edgW[str(181)+" "+str(87)] = 853
    edgW[str(88)+" "+str(136)] = 149
    edgW[str(136)+" "+str(88)] = 149
    edgW[str(88)+" "+str(175)] = 665
    edgW[str(175)+" "+str(88)] = 665
    edgW[str(88)+" "+str(176)] = 896
    edgW[str(176)+" "+str(88)] = 896
    edgW[str(89)+" "+str(125)] = 373
    edgW[str(125)+" "+str(89)] = 373
    edgW[str(89)+" "+str(131)] = 377
    edgW[str(131)+" "+str(89)] = 377
    edgW[str(89)+" "+str(141)] = 862
    edgW[str(141)+" "+str(89)] = 862
    edgW[str(89)+" "+str(182)] = 605
    edgW[str(182)+" "+str(89)] = 605
    edgW[str(90)+" "+str(95)] = 299
    edgW[str(95)+" "+str(90)] = 299
    edgW[str(90)+" "+str(199)] = 661
    edgW[str(199)+" "+str(90)] = 661
    edgW[str(91)+" "+str(128)] = 594
    edgW[str(128)+" "+str(91)] = 594
    edgW[str(91)+" "+str(134)] = 789
    edgW[str(134)+" "+str(91)] = 789
    edgW[str(91)+" "+str(164)] = 173
    edgW[str(164)+" "+str(91)] = 173
    edgW[str(91)+" "+str(175)] = 868
    edgW[str(175)+" "+str(91)] = 868
    edgW[str(91)+" "+str(198)] = 212
    edgW[str(198)+" "+str(91)] = 212
    edgW[str(92)+" "+str(200)] = 505
    edgW[str(200)+" "+str(92)] = 505
    edgW[str(93)+" "+str(116)] = 542
    edgW[str(116)+" "+str(93)] = 542
    edgW[str(93)+" "+str(196)] = 201
    edgW[str(196)+" "+str(93)] = 201
    edgW[str(94)+" "+str(97)] = 230
    edgW[str(97)+" "+str(94)] = 230
    edgW[str(94)+" "+str(109)] = 614
    edgW[str(109)+" "+str(94)] = 614
    edgW[str(95)+" "+str(119)] = 433
    edgW[str(119)+" "+str(95)] = 433
    edgW[str(96)+" "+str(103)] = 16
    edgW[str(103)+" "+str(96)] = 16
    edgW[str(97)+" "+str(136)] = 284
    edgW[str(136)+" "+str(97)] = 284
    edgW[str(97)+" "+str(138)] = 682
    edgW[str(138)+" "+str(97)] = 682
    edgW[str(97)+" "+str(151)] = 645
    edgW[str(151)+" "+str(97)] = 645
    edgW[str(97)+" "+str(158)] = 588
    edgW[str(158)+" "+str(97)] = 588
    edgW[str(97)+" "+str(171)] = 58
    edgW[str(171)+" "+str(97)] = 58
    edgW[str(97)+" "+str(187)] = 799
    edgW[str(187)+" "+str(97)] = 799
    edgW[str(99)+" "+str(127)] = 302
    edgW[str(127)+" "+str(99)] = 302
    edgW[str(99)+" "+str(189)] = 493
    edgW[str(189)+" "+str(99)] = 493
    edgW[str(100)+" "+str(143)] = 607
    edgW[str(143)+" "+str(100)] = 607
    edgW[str(100)+" "+str(148)] = 932
    edgW[str(148)+" "+str(100)] = 932
    edgW[str(101)+" "+str(105)] = 295
    edgW[str(105)+" "+str(101)] = 295
    edgW[str(101)+" "+str(106)] = 673
    edgW[str(106)+" "+str(101)] = 673
    edgW[str(101)+" "+str(166)] = 250
    edgW[str(166)+" "+str(101)] = 250
    edgW[str(101)+" "+str(178)] = 229
    edgW[str(178)+" "+str(101)] = 229
    edgW[str(101)+" "+str(183)] = 673
    edgW[str(183)+" "+str(101)] = 673
    edgW[str(102)+" "+str(124)] = 577
    edgW[str(124)+" "+str(102)] = 577
    edgW[str(102)+" "+str(186)] = 781
    edgW[str(186)+" "+str(102)] = 781
    edgW[str(103)+" "+str(104)] = 362
    edgW[str(104)+" "+str(103)] = 362
    edgW[str(104)+" "+str(129)] = 990
    edgW[str(129)+" "+str(104)] = 990
    edgW[str(104)+" "+str(130)] = 269
    edgW[str(130)+" "+str(104)] = 269
    edgW[str(105)+" "+str(193)] = 38
    edgW[str(193)+" "+str(105)] = 38
    edgW[str(106)+" "+str(121)] = 569
    edgW[str(121)+" "+str(106)] = 569
    edgW[str(106)+" "+str(153)] = 693
    edgW[str(153)+" "+str(106)] = 693
    edgW[str(106)+" "+str(184)] = 744
    edgW[str(184)+" "+str(106)] = 744
    edgW[str(107)+" "+str(194)] = 906
    edgW[str(194)+" "+str(107)] = 906
    edgW[str(109)+" "+str(126)] = 111
    edgW[str(126)+" "+str(109)] = 111
    edgW[str(109)+" "+str(140)] = 686
    edgW[str(140)+" "+str(109)] = 686
    edgW[str(109)+" "+str(150)] = 316
    edgW[str(150)+" "+str(109)] = 316
    edgW[str(111)+" "+str(190)] = 992
    edgW[str(190)+" "+str(111)] = 992
    edgW[str(112)+" "+str(178)] = 18
    edgW[str(178)+" "+str(112)] = 18
    edgW[str(113)+" "+str(122)] = 10
    edgW[str(122)+" "+str(113)] = 10
    edgW[str(114)+" "+str(120)] = 447
    edgW[str(120)+" "+str(114)] = 447
    edgW[str(114)+" "+str(140)] = 650
    edgW[str(140)+" "+str(114)] = 650
    edgW[str(115)+" "+str(128)] = 478
    edgW[str(128)+" "+str(115)] = 478
    edgW[str(120)+" "+str(132)] = 349
    edgW[str(132)+" "+str(120)] = 349
    edgW[str(120)+" "+str(142)] = 529
    edgW[str(142)+" "+str(120)] = 529
    edgW[str(120)+" "+str(143)] = 492
    edgW[str(143)+" "+str(120)] = 492
    edgW[str(120)+" "+str(177)] = 200
    edgW[str(177)+" "+str(120)] = 200
    edgW[str(121)+" "+str(148)] = 328
    edgW[str(148)+" "+str(121)] = 328
    edgW[str(121)+" "+str(196)] = 212
    edgW[str(196)+" "+str(121)] = 212
    edgW[str(122)+" "+str(168)] = 537
    edgW[str(168)+" "+str(122)] = 537
    edgW[str(123)+" "+str(177)] = 65
    edgW[str(177)+" "+str(123)] = 65
    edgW[str(124)+" "+str(130)] = 325
    edgW[str(130)+" "+str(124)] = 325
    edgW[str(124)+" "+str(159)] = 629
    edgW[str(159)+" "+str(124)] = 629
    edgW[str(124)+" "+str(200)] = 674
    edgW[str(200)+" "+str(124)] = 674
    edgW[str(125)+" "+str(179)] = 901
    edgW[str(179)+" "+str(125)] = 901
    edgW[str(125)+" "+str(182)] = 660
    edgW[str(182)+" "+str(125)] = 660
    edgW[str(127)+" "+str(191)] = 20
    edgW[str(191)+" "+str(127)] = 20
    edgW[str(129)+" "+str(200)] = 690
    edgW[str(200)+" "+str(129)] = 690
    edgW[str(130)+" "+str(140)] = 173
    edgW[str(140)+" "+str(130)] = 173
    edgW[str(132)+" "+str(190)] = 732
    edgW[str(190)+" "+str(132)] = 732
    edgW[str(133)+" "+str(135)] = 975
    edgW[str(135)+" "+str(133)] = 975
    edgW[str(134)+" "+str(146)] = 33
    edgW[str(146)+" "+str(134)] = 33
    edgW[str(134)+" "+str(147)] = 284
    edgW[str(147)+" "+str(134)] = 284
    edgW[str(134)+" "+str(149)] = 470
    edgW[str(149)+" "+str(134)] = 470
    edgW[str(136)+" "+str(184)] = 652
    edgW[str(184)+" "+str(136)] = 652
    edgW[str(137)+" "+str(167)] = 415
    edgW[str(167)+" "+str(137)] = 415
    edgW[str(137)+" "+str(169)] = 5
    edgW[str(169)+" "+str(137)] = 5
    edgW[str(139)+" "+str(145)] = 448
    edgW[str(145)+" "+str(139)] = 448
    edgW[str(139)+" "+str(148)] = 226
    edgW[str(148)+" "+str(139)] = 226
    edgW[str(141)+" "+str(152)] = 483
    edgW[str(152)+" "+str(141)] = 483
    edgW[str(141)+" "+str(162)] = 695
    edgW[str(162)+" "+str(141)] = 695
    edgW[str(141)+" "+str(167)] = 219
    edgW[str(167)+" "+str(141)] = 219
    edgW[str(142)+" "+str(147)] = 832
    edgW[str(147)+" "+str(142)] = 832
    edgW[str(142)+" "+str(164)] = 915
    edgW[str(164)+" "+str(142)] = 915
    edgW[str(142)+" "+str(191)] = 207
    edgW[str(191)+" "+str(142)] = 207
    edgW[str(143)+" "+str(177)] = 265
    edgW[str(177)+" "+str(143)] = 265
    edgW[str(143)+" "+str(194)] = 949
    edgW[str(194)+" "+str(143)] = 949
    edgW[str(144)+" "+str(151)] = 488
    edgW[str(151)+" "+str(144)] = 488
    edgW[str(144)+" "+str(174)] = 197
    edgW[str(174)+" "+str(144)] = 197
    edgW[str(144)+" "+str(192)] = 445
    edgW[str(192)+" "+str(144)] = 445
    edgW[str(145)+" "+str(172)] = 888
    edgW[str(172)+" "+str(145)] = 888
    edgW[str(147)+" "+str(152)] = 628
    edgW[str(152)+" "+str(147)] = 628
    edgW[str(148)+" "+str(179)] = 627
    edgW[str(179)+" "+str(148)] = 627
    edgW[str(148)+" "+str(184)] = 914
    edgW[str(184)+" "+str(148)] = 914
    edgW[str(150)+" "+str(160)] = 41
    edgW[str(160)+" "+str(150)] = 41
    edgW[str(150)+" "+str(178)] = 169
    edgW[str(178)+" "+str(150)] = 169
    edgW[str(151)+" "+str(200)] = 31
    edgW[str(200)+" "+str(151)] = 31
    edgW[str(155)+" "+str(196)] = 210
    edgW[str(196)+" "+str(155)] = 210
    edgW[str(160)+" "+str(169)] = 210
    edgW[str(169)+" "+str(160)] = 210
    edgW[str(171)+" "+str(173)] = 960
    edgW[str(173)+" "+str(171)] = 960
    edgW[str(172)+" "+str(174)] = 39
    edgW[str(174)+" "+str(172)] = 39
    edgW[str(173)+" "+str(188)] = 50
    edgW[str(188)+" "+str(173)] = 50
    edgW[str(174)+" "+str(185)] = 58
    edgW[str(185)+" "+str(174)] = 58
    edgW[str(174)+" "+str(186)] = 785
    edgW[str(186)+" "+str(174)] = 785
    edgW[str(176)+" "+str(185)] = 468
    edgW[str(185)+" "+str(176)] = 468
    edgW[str(176)+" "+str(187)] = 527
    edgW[str(187)+" "+str(176)] = 527
    edgW[str(181)+" "+str(196)] = 229
    edgW[str(196)+" "+str(181)] = 229
    edgW[str(183)+" "+str(192)] = 879
    edgW[str(192)+" "+str(183)] = 879
    edgW[str(189)+" "+str(193)] = 632
    edgW[str(193)+" "+str(189)] = 632

    return edgW

def getGraph3():
    GT = [[],
            [2, 44, 95, 102, 162, 166],
            [1, 3, 6, 119, 156],
            [2, 4, 8, 26, 57, 65, 69, 108, 123, 170],
            [3, 5, 7, 12, 21, 68, 74, 198],
            [4, 10, 18, 23, 55, 81, 175],
            [2, 17, 46, 52, 71, 92],
            [4, 15, 19, 25, 85, 100, 130, 195],
            [3, 9, 11, 13, 16, 38, 40, 115, 131],
            [8, 14, 34, 42, 51, 191],
            [5, 19, 30, 41, 86, 122],
            [8, 56, 98, 99, 117, 129, 132, 176],
            [4, 35, 49, 139, 161],
            [8, 27, 51, 83, 185],
            [9, 145],
            [7, 27, 63, 79, 148, 161],
            [8, 108],
            [6, 27, 71, 73, 109, 117, 149, 190],
            [5, 20, 29, 128],
            [7, 10, 32, 37, 53, 54, 197],
            [18, 22, 24, 116, 134],
            [4, 39, 91, 126, 142, 173],
            [20, 42, 61, 147, 152, 177],
            [5, 33, 111, 150],
            [20, 31, 54, 82, 127, 187],
            [7, 28, 48, 73],
            [3, 48, 179, 196],
            [13, 15, 17, 30, 44],
            [25, 83, 93, 118, 138],
            [18, 45, 53, 95, 124, 159],
            [10, 27, 31, 64, 77, 133, 163, 174, 177],
            [24, 30, 109, 110, 116],
            [19, 113],
            [23, 125, 126],
            [9, 47, 48, 51, 78, 93, 155],
            [12, 36, 58, 89, 90, 93, 139, 156, 172, 173],
            [35],
            [19, 43, 126, 144],
            [8, 55],
            [21, 59, 119, 141, 191],
            [8, 62, 100, 120, 157],
            [10, 66],
            [9, 22, 60, 76, 104, 142, 149, 162, 181],
            [37, 46, 96, 124, 196],
            [1, 27, 64],
            [29],
            [6, 43, 106, 112, 182],
            [34, 50, 78, 156, 198],
            [25, 26, 34],
            [12, 57, 99, 154, 176, 199],
            [47, 81, 88, 99, 126, 157, 169],
            [9, 13, 34, 55, 70, 155],
            [6, 67, 72, 155, 172],
            [19, 29, 102],
            [19, 24, 97, 138],
            [5, 38, 51, 115, 136, 200],
            [11],
            [3, 49],
            [35, 59, 157],
            [39, 58, 135],
            [42, 136, 194, 200],
            [22, 169],
            [40, 110, 180],
            [15, 87, 114, 140, 184],
            [30, 44, 151, 165],
            [3, 84, 95, 188],
            [41, 75, 149, 152],
            [52, 180],
            [4, 74, 83, 157, 164],
            [3, 121, 122, 137],
            [51],
            [6, 17, 84, 109, 148, 166],
            [52, 116, 197, 198],
            [17, 25, 75, 89],
            [4, 68, 141],
            [66, 73, 107, 118],
            [42, 78, 81, 139, 162, 167],
            [30, 141, 157, 165, 179, 186],
            [34, 47, 76, 121],
            [15, 80, 158],
            [79, 154],
            [5, 50, 76, 161],
            [24, 101],
            [13, 28, 68, 139],
            [65, 71, 110],
            [7, 94, 97, 182],
            [10, 126, 127, 134, 146],
            [63, 181],
            [50, 136, 175, 176],
            [35, 73, 125, 131, 141, 182],
            [35, 95, 199],
            [21, 128, 134, 164, 175, 198],
            [6, 200],
            [28, 34, 35, 116, 196],
            [85, 97, 109],
            [1, 29, 65, 90, 119],
            [43, 103],
            [54, 85, 94, 136, 138, 151, 158, 171, 187],
            [11],
            [11, 49, 50, 127, 189],
            [7, 40, 143, 148],
            [82, 105, 106, 166, 178, 183],
            [1, 53, 124, 186],
            [96, 104],
            [42, 103, 129, 130],
            [101, 193],
            [46, 101, 121, 153, 184],
            [75, 194],
            [3, 16],
            [17, 31, 71, 94, 126, 140, 150],
            [31, 62, 84],
            [23, 190],
            [46, 178],
            [32, 122],
            [63, 120, 140],
            [8, 55, 128],
            [20, 31, 72, 93],
            [11, 17],
            [28, 75],
            [2, 39, 95],
            [40, 114, 132, 142, 143, 177],
            [69, 78, 106, 148, 196],
            [10, 69, 113, 168],
            [3, 177],
            [29, 43, 102, 130, 159, 200],
            [33, 89, 179, 182],
            [21, 33, 37, 50, 86, 109],
            [24, 86, 99, 191],
            [18, 91, 115],
            [11, 104, 200],
            [7, 104, 124, 140],
            [8, 89],
            [11, 120, 190],
            [30, 135],
            [20, 86, 91, 146, 147, 149],
            [59, 133],
            [55, 60, 88, 97, 184],
            [69, 167, 169],
            [28, 54, 97],
            [12, 35, 76, 83, 145, 148],
            [63, 109, 114, 130],
            [39, 74, 77, 89, 152, 162, 167],
            [21, 42, 120, 147, 164, 191],
            [100, 120, 177, 194],
            [37, 151, 174, 192],
            [14, 139, 172],
            [86, 134],
            [22, 134, 142, 152],
            [15, 71, 100, 121, 139, 179, 184],
            [17, 42, 66, 134],
            [23, 109, 160, 178],
            [64, 97, 144, 200],
            [22, 66, 141, 147],
            [106],
            [49, 80],
            [34, 51, 52, 196],
            [2, 35, 47],
            [40, 50, 58, 68, 77],
            [79, 97],
            [29, 124],
            [150, 169],
            [12, 15, 81],
            [1, 42, 76, 141],
            [30],
            [68, 91, 142],
            [64, 77],
            [1, 71, 101],
            [76, 137, 141],
            [122],
            [50, 61, 137, 160],
            [3],
            [97, 173],
            [35, 52, 145, 174],
            [21, 35, 171, 188],
            [30, 144, 172, 185, 186],
            [5, 88, 91],
            [11, 49, 88, 185, 187],
            [22, 30, 120, 123, 143],
            [101, 112, 150],
            [26, 77, 125, 148],
            [62, 67],
            [42, 87, 196],
            [46, 85, 89, 125],
            [101, 192],
            [63, 106, 136, 148],
            [13, 174, 176],
            [77, 102, 174],
            [24, 97, 176],
            [65, 173],
            [99, 193],
            [17, 111, 132],
            [9, 39, 127, 142],
            [144, 183],
            [105, 189],
            [60, 107, 143],
            [7],
            [26, 43, 93, 121, 155, 181],
            [19, 72],
            [4, 47, 72, 91],
            [49, 90],
            [55, 60, 92, 124, 129, 151]
        ]
    return GT

'''
Greedy tipa algoritms

Ieejā saņem atrastos ciklus un ceļu svaru vārdnīcu
Iet cauri katram ciklam, 
    izvēlas ciklā ceļu ar mazāko svaru, ja ciklā jau kāds ceļs ir izvēlēts, tad izlaiž konkrēto ciklu

Globālajā mainīgajā 'chosen_edge_map' tiek iekodēti izvēlētie ceļi
'''
def calcSum(res, edgW):

    global chosen_edge_map
    sum = 0

    chosenEdges = {}
    res2 = []

    for entr in res:
        if(len(entr) == 2):
            continue

        first = entr[0]
        entr.append(first)
        res2.append(entr)

    for st in res2:
        edgK = []

        for i in range(len(st)):
            if i == len(st) - 1:
                break

            k = str(st[i]) + " " + str(st[i+1])
            k2 = str(st[i+1]) + " " + str(st[i])
            edgK.append(k)
            edgK.append(k2)

        minV = 100000
        minK = ""
        skip = False

        for i in range(len(edgK)):
            k = edgK[i]
            if k in chosenEdges:
                skip = True
                break

        if not skip:
            for i in range(len(edgK)):
                k = edgK[i]
                kVal = edgW[k]

                if minV > kVal:
                    minV = kVal
                    minK = k

            chosenEdges[minK] = 1
            chosen_edge_map[minK] = 1
            sum = sum + minV

    print(sum)
    return sum


G = []
cycles = []

point_stack = []
marked = []
marked_stack = []
find_len = 3

cycles_at_len = {}
chosen_edge_map = {}

'''
Iteratīvs Tarjan Enumaration of the Elementary Circuits algoritms ar papildinājumiem konkrētajam uzdevumam
Paša Tarjan algoritma aprakstu atradu šeit: 
http://epubs.siam.org/doi/pdf/10.1137/0202017
https://www.researchgate.net/profile/Roger_Sargent/publication/272481243_Tarjan-1973-_paper_used_in_TfCAPD_course/links/54e513d50cf276cec1731a86/Tarjan-1973-paper-used-in-TfCAPD-course.pdf?origin=publication_list

Rekursīvo realizāciju Pythonā atradu šeit:
https://github.com/janvdl/python_tarjan/blob/master/tarjan.py

Par bāzi šai funkcijai izmantoju rekursīvu Python implementāciju.

Tā kā sākumā eksperimentējot iekš Java ar Johnsons algoritma (kas bāzēts uz Tarjan) realizāciju saskāros, ka
Java nespēja apstrādāt daudzās rekursijas, tad pārrakstīju rekursīvo implementāciju uz iteratīvu variantu, nezinu vai kaut ko uzlaboja Pythonā...

Papildus modificēju metodi, lai tiktu meklēti cikli noteiktā garumā, attiecīgi, visas dziļākās meklēšanas tiek apturētas
visi risinājumi, kas īsāki par meklēto garumu netiek pievienoti pie atrastajiem cikliem

Pielikta pārbaude, kad notiek meklēšana, ja pašreiz apskatāmais ceļs jau ietilpst izvēlēto ceļu sarakstā no iepriekšējiem atrastajiem cikliem (iegūti ar šo pašu metodi, bet tika meklēti īsāka garuma cikli), tad tiek
izlaistas meklēšanas ar risinājumiem, kas jau satur šo ceļu

Rezultātā metode atgriež visus atrastos ciklus meklētajā garumā, kas iziet no virsotnes 's' un kuri atbilst papildus pieliktajiem ierobežojumiem
Tā kā atrastajos ciklos tiek izlaista pēdējā ceļa norāde (atpakaļ uz starta virsotni), tad jāatceras šo ceļu ņemt vērā, kad apstrādā atrastos ciklus
'''
def tarjan(s, v):
    global cycles, find_len, chosen_edge_map

    if s == 0:
        return

    vStack = []
    fStack = []
    indxStack = []

    f = False
    g = False

    while True:

        if len(vStack) > 0:
            v = vStack.pop()
            g = f
            f = fStack.pop()
            f = f or g
            indx = indxStack.pop()
            indx = indx + 1
        else:
            f = False
            point_stack.append(v)
            marked[v] = True
            marked_stack.append(v)
            indx = 0

        while indx < len(G[v]):
            w = G[v][indx]
            key = str(v) + " " + str(w)
            key2 = str(w) + " " + str(v)

            if w<s:
                G[w] = 0
            elif len(point_stack) > find_len:
                f = False
                break
            elif key in chosen_edge_map or key2 in chosen_edge_map:
                f = True
                
            elif w == s and len(point_stack) != find_len:
                f = True
                indx = indx + 1
                continue
            elif w == s and len(point_stack) == find_len:
                #cycles.append(list(deepcopy(point_stack)))
                cycles.append(list(point_stack))
                f = True
            elif marked[w] == False:
                vStack.append(v)
                fStack.append(f)
                indxStack.append(indx)

                v = w

                f = False
                point_stack.append(v)
                marked[v] = True
                marked_stack.append(v)
                indx = 0

                continue

            indx = indx + 1
            
        if f == True:
            while marked_stack[len(marked_stack) - 1] != v:
                u = marked_stack.pop()
                marked[u] = False
            marked_stack.pop()
            marked[v] = False
        
        point_stack.pop()

        if len(vStack) > 0:
            continue
            
        else:
            break

'''
Ieejas metode Tarjan algoritmam

Par bāzi šai metodei izmantoju Python algoritma realizāciju, skatīt metodes 'tarjan' aprakstu.
Papildināju ar to, ka metodei padod papildus garuma parametru, kas noteiks, kāda garuma ciklus metode 'tarjan' meklēs.

Viens šīs metodes izsaukums apstrādās katru grafa virsotni vienu reizi, kur no katras virsotnes tiks meklēti cikli no šīs virsotnes

'''        
def entry_tarjan(G_, l):
    global G, cycles, marked, find_len
    G = deepcopy(G_)

    marked = [False for x in range(0, len(G_))]
    
    for i in range(len(G)):
        # pirmais elements ir 'dummy', lai varētu strādāt ar ieejas indeksiem pa taisno
        if i == 0:
            continue

        find_len = l
        tarjan(i, i)
        while marked_stack:
            u = marked_stack.pop()
            marked[u] = False
    return cycles

'''
Tā kā metode 'tarjan' izmanto globālos mainīgos, tad nepieciešams viņus 'satīrīt' pēc katras 'entry_tarjan'
darbināšanas
'''
def resetGlobals():
    global cycles, point_stack, marked, marked_stack
    cycles = []

    point_stack = []
    marked = []
    marked_stack = []

def resetChosenEdgeMap():
    global chosen_edge_map

    chosen_edge_map = {}

def getCosenEdgeMap():
    global chosen_edge_map
    return chosen_edge_map

'''
GT = getGraph3()
foundSum = 0

GLen = len(GT) - 1
cur = 3

foundCycles = []

while cur <= GLen:
    cycl = entry_tarjan(GT, cur)

    foundCycles = foundCycles + cycl
    #printList(foundCycles)
    if len(cycl) == 0:
        pass
    else:
        chosen_edge_map = {}
        foundSum = calcSum(list(deepcopy(foundCycles)), getWeights3())

    cur = cur + 1
    resetGlobals()

print(len(foundCycles))
outputResult(foundSum, chosen_edge_map)
'''