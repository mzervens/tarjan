from copy import deepcopy

G = []
cycles = []

point_stack = []
marked = []
marked_stack = []
used_roads = {}
used_points = {}
added = 0
startLen = 3

def tarjanIter(s, v):
    global cycles

    if s == 0:
        return

    f = False
    point_stack.append(v)
    marked[v] = True
    marked_stack.append(v)
    reloop = False

    prevVStack = []
    prevFStack = []

    while True:

        for w in G[v]:
            if w<s:
                G[w] = 0
            elif w == s and len(point_stack) !=2:
                cycles.append(list(deepcopy(point_stack)))
                f = True
            elif marked[w] == False:
                point_stack.append(w)
                marked[w] = True
                marked_stack.append(w)
                reloop = True
                prevVStack.append(v)
                prevFStack.append(f)
                v = w
                break
            else:
                f = False

        if reloop:
            reloop = False
            continue
            
        if f == True:
            while marked_stack[len(marked_stack) - 1] != v:
                u = marked_stack.pop()
                marked[u] = False
            marked_stack.pop()
            marked[v] = False
        
        point_stack.pop()
        if len(prevVStack) == 0:
            break
        else:
            v = prevVStack.pop()
            f = f or prevFStack.pop()


def tarjan(s, v):
    global cycles, used_roads, edgW, added, used_points
    f = False



    #if len(used_roads) >= len(edgW):
    #    return

    point_stack.append(v)
    marked[v] = True
    marked_stack.append(v)
    for w in G[v]:
        if w<s:
            G[w] = 0
        elif w == s and len(point_stack) !=2:
            cycles.append(list(deepcopy(point_stack)))
            added = added + 1

            for i in range(len(point_stack)):
                if(i != len(point_stack)-1):
                    n1 = point_stack[i]
                    n2 = point_stack[i+1]
                    used_roads[str(n1)+" "+str(n2)] = 1
                    used_roads[str(n2)+" "+str(n1)] = 1
                else:
                    n1 = point_stack[i]
                    n2 = point_stack[0]
                    used_roads[str(n1)+" "+str(n2)] = 1
                    used_roads[str(n2)+" "+str(n1)] = 1

            f = True
        elif marked[w] == False :#and not ((str(v)+" "+str(w) in used_roads) ):
            g = tarjan(s,w)
            f = f or g
            
    if f == True:
        while marked_stack[len(marked_stack) - 1] != v:
            u = marked_stack.pop()
            marked[u] = False
        marked_stack.pop()
        marked[v] = False
        
    point_stack.pop()
    return f
        
def entry_tarjan(G_):
    global G, cycles, marked, point_stack, used_roads, added, used_points
    G = deepcopy(G_)

    marked = [False for x in range(0, len(G_))]
    
    for i in range(len(G)):

        print("finding " + str(i))
        tarjanIter(i, i)
        while marked_stack:
            u = marked_stack.pop()
            marked[u] = False
    
        point_stack = []
        used_roads = {}
        used_points = {}
        added = 0

    return cycles

'''
GT = [[],
     [ 2, 3, 4, 5, 7, 9, 10], 
     [ 1, 3, 4, 6, 7, 8, 10],
     [ 10, 1, 4, 2, 6, 7, 8],
     [ 2, 1, 3, 6, 9],
     [ 1, 2, 7, 9],
     [ 10, 2, 3, 4, 7, 8, 9],
     [ 1, 6, 5, 3, 2, 8],
     [ 10, 2, 3, 6, 7, 9],
     [ 1, 4, 5, 6, 8],
     [ 3, 6, 8, 2, 1]]


GT = [[],
[2, 44, 95, 102, 162, 166],
[1, 3, 6, 119, 156],
[2, 4, 8, 26, 57, 65, 69, 108, 123, 170],
[3, 5, 7, 12, 21, 68, 74, 198],
[4, 10, 18, 23, 55, 81, 175],
[2, 17, 46, 52, 71, 92],
[4, 15, 19, 25, 85, 100, 130, 195],
[3, 9, 11, 13, 16, 38, 40, 115, 131],
[8, 14, 34, 42, 51, 191],
[5, 19, 30, 41, 86, 122],
[8, 56, 98, 99, 117, 129, 132, 176],
[4, 35, 49, 139, 161],
[8, 27, 51, 83, 185],
[9, 145],
[7, 27, 63, 79, 148, 161],
[8, 108],
[6, 27, 71, 73, 109, 117, 149, 190],
[5, 20, 29, 128],
[7, 10, 32, 37, 53, 54, 197],
[18, 22, 24, 116, 134],
[4, 39, 91, 126, 142, 173],
[20, 42, 61, 147, 152, 177],
[5, 33, 111, 150],
[20, 31, 54, 82, 127, 187],
[7, 28, 48, 73],
[3, 48, 179, 196],
[13, 15, 17, 30, 44],
[25, 83, 93, 118, 138],
[18, 45, 53, 95, 124, 159],
[10, 27, 31, 64, 77, 133, 163, 174, 177],
[24, 30, 109, 110, 116],
[19, 113],
[23, 125, 126],
[9, 47, 48, 51, 78, 93, 155],
[12, 36, 58, 89, 90, 93, 139, 156, 172, 173],
[35],
[19, 43, 126, 144],
[8, 55],
[21, 59, 119, 141, 191],
[8, 62, 100, 120, 157],
[10, 66],
[9, 22, 60, 76, 104, 142, 149, 162, 181],
[37, 46, 96, 124, 196],
[1, 27, 64],
[29],
[6, 43, 106, 112, 182],
[34, 50, 78, 156, 198],
[25, 26, 34],
[12, 57, 99, 154, 176, 199],
[47, 81, 88, 99, 126, 157, 169],
[9, 13, 34, 55, 70, 155],
[6, 67, 72, 155, 172],
[19, 29, 102],
[19, 24, 97, 138],
[5, 38, 51, 115, 136, 200],
[11],
[3, 49],
[35, 59, 157],
[39, 58, 135],
[42, 136, 194, 200],
[22, 169],
[40, 110, 180],
[15, 87, 114, 140, 184],
[30, 44, 151, 165],
[3, 84, 95, 188],
[41, 75, 149, 152],
[52, 180],
[4, 74, 83, 157, 164],
[3, 121, 122, 137],
[51],
[6, 17, 84, 109, 148, 166],
[52, 116, 197, 198],
[17, 25, 75, 89],
[4, 68, 141],
[66, 73, 107, 118],
[42, 78, 81, 139, 162, 167],
[30, 141, 157, 165, 179, 186],
[34, 47, 76, 121],
[15, 80, 158],
[79, 154],
[5, 50, 76, 161],
[24, 101],
[13, 28, 68, 139],
[65, 71, 110],
[7, 94, 97, 182],
[10, 126, 127, 134, 146],
[63, 181],
[50, 136, 175, 176],
[35, 73, 125, 131, 141, 182],
[35, 95, 199],
[21, 128, 134, 164, 175, 198],
[6, 200],
[28, 34, 35, 116, 196],
[85, 97, 109],
[1, 29, 65, 90, 119],
[43, 103],
[54, 85, 94, 136, 138, 151, 158, 171, 187],
[11],
[11, 49, 50, 127, 189],
[7, 40, 143, 148],
[82, 105, 106, 166, 178, 183],
[1, 53, 124, 186],
[96, 104],
[42, 103, 129, 130],
[101, 193],
[46, 101, 121, 153, 184],
[75, 194],
[3, 16],
[17, 31, 71, 94, 126, 140, 150],
[31, 62, 84],
[23, 190],
[46, 178],
[32, 122],
[63, 120, 140],
[8, 55, 128],
[20, 31, 72, 93],
[11, 17],
[28, 75],
[2, 39, 95],
[40, 114, 132, 142, 143, 177],
[69, 78, 106, 148, 196],
[10, 69, 113, 168],
[3, 177],
[29, 43, 102, 130, 159, 200],
[33, 89, 179, 182],
[21, 33, 37, 50, 86, 109],
[24, 86, 99, 191],
[18, 91, 115],
[11, 104, 200],
[7, 104, 124, 140],
[8, 89],
[11, 120, 190],
[30, 135],
[20, 86, 91, 146, 147, 149],
[59, 133],
[55, 60, 88, 97, 184],
[69, 167, 169],
[28, 54, 97],
[12, 35, 76, 83, 145, 148],
[63, 109, 114, 130],
[39, 74, 77, 89, 152, 162, 167],
[21, 42, 120, 147, 164, 191],
[100, 120, 177, 194],
[37, 151, 174, 192],
[14, 139, 172],
[86, 134],
[22, 134, 142, 152],
[15, 71, 100, 121, 139, 179, 184],
[17, 42, 66, 134],
[23, 109, 160, 178],
[64, 97, 144, 200],
[22, 66, 141, 147],
[106],
[49, 80],
[34, 51, 52, 196],
[2, 35, 47],
[40, 50, 58, 68, 77],
[79, 97],
[29, 124],
[150, 169],
[12, 15, 81],
[1, 42, 76, 141],
[30],
[68, 91, 142],
[64, 77],
[1, 71, 101],
[76, 137, 141],
[122],
[50, 61, 137, 160],
[3],
[97, 173],
[35, 52, 145, 174],
[21, 35, 171, 188],
[30, 144, 172, 185, 186],
[5, 88, 91],
[11, 49, 88, 185, 187],
[22, 30, 120, 123, 143],
[101, 112, 150],
[26, 77, 125, 148],
[62, 67],
[42, 87, 196],
[46, 85, 89, 125],
[101, 192],
[63, 106, 136, 148],
[13, 174, 176],
[77, 102, 174],
[24, 97, 176],
[65, 173],
[99, 193],
[17, 111, 132],
[9, 39, 127, 142],
[144, 183],
[105, 189],
[60, 107, 143],
[7],
[26, 43, 93, 121, 155, 181],
[19, 72],
[4, 47, 72, 91],
[49, 90],
[55, 60, 92, 124, 129, 151]
]




'''



edgW = {}
chosenEdges = {}
'''
GT = [[],
      [2, 3, 4],
      [1, 3, 4],
      [1, 2],
      [1, 2]
    
    ]

edgW[str(1)+" "+str(3)] = 5
edgW[str(3)+" "+str(1)] = 5

edgW[str(2)+" "+str(3)] = 7
edgW[str(3)+" "+str(2)] = 7

edgW[str(1)+" "+str(2)] = 9
edgW[str(2)+" "+str(1)] = 9

edgW[str(4)+" "+str(2)] = 10
edgW[str(2)+" "+str(4)] = 10

edgW[str(4)+" "+str(1)] = 2
edgW[str(1)+" "+str(4)] = 2


'''
GT = [[],
[2, 3, 4, 5, 7, 9, 10],
[1, 3, 4, 5, 6, 7, 8, 10],
[1, 10, 2, 4, 6, 7, 8],
[1, 2, 3, 6, 9],
[1, 2, 7, 9],
[10, 2, 3, 4, 7, 8, 9],
[1, 2, 3, 5, 6, 8],
[10, 2, 3, 6, 7, 9],
[1, 4, 5, 6, 8],
[3, 6, 8, 1, 2],
]
edgW[str(1)+" "+str(2)] = 155
edgW[str(2)+" "+str(1)] = 155
edgW[str(1)+" "+str(3)] = 751
edgW[str(3)+" "+str(1)] = 751
edgW[str(1)+" "+str(4)] = 726
edgW[str(4)+" "+str(1)] = 726
edgW[str(1)+" "+str(5)] = 89
edgW[str(5)+" "+str(1)] = 89
edgW[str(1)+" "+str(7)] = 63
edgW[str(7)+" "+str(1)] = 63
edgW[str(1)+" "+str(9)] = 700
edgW[str(9)+" "+str(1)] = 700
edgW[str(1)+" "+str(10)] = 336
edgW[str(10)+" "+str(1)] = 336


edgW[str(2)+" "+str(3)] = 180
edgW[str(3)+" "+str(2)] = 180
edgW[str(2)+" "+str(4)] = 66
edgW[str(4)+" "+str(2)] = 66
edgW[str(2)+" "+str(5)] = 409
edgW[str(5)+" "+str(2)] = 409
edgW[str(2)+" "+str(6)] = 579
edgW[str(6)+" "+str(2)] = 579
edgW[str(2)+" "+str(7)] = 305
edgW[str(7)+" "+str(2)] = 305
edgW[str(2)+" "+str(8)] = 555
edgW[str(8)+" "+str(2)] = 555
edgW[str(2)+" "+str(10)] = 442
edgW[str(10)+" "+str(2)] = 442


edgW[str(3)+" "+str(10)] = 799
edgW[str(10)+" "+str(3)] = 799


edgW[str(3)+" "+str(4)] = 889
edgW[str(4)+" "+str(3)] = 889
edgW[str(3)+" "+str(6)] = 714
edgW[str(6)+" "+str(3)] = 714
edgW[str(3)+" "+str(7)] = 414
edgW[str(7)+" "+str(3)] = 414
edgW[str(3)+" "+str(8)] = 252
edgW[str(8)+" "+str(3)] = 252






edgW[str(4)+" "+str(6)] = 772
edgW[str(6)+" "+str(4)] = 772
edgW[str(4)+" "+str(9)] = 517
edgW[str(9)+" "+str(4)] = 517




edgW[str(5)+" "+str(7)] = 789
edgW[str(7)+" "+str(5)] = 789
edgW[str(5)+" "+str(9)] = 323
edgW[str(9)+" "+str(5)] = 323
edgW[str(6)+" "+str(10)] = 472
edgW[str(10)+" "+str(6)] = 472






edgW[str(6)+" "+str(7)] = 374
edgW[str(7)+" "+str(6)] = 374
edgW[str(6)+" "+str(8)] = 923
edgW[str(8)+" "+str(6)] = 923
edgW[str(6)+" "+str(9)] = 502
edgW[str(9)+" "+str(6)] = 502










edgW[str(7)+" "+str(8)] = 705
edgW[str(8)+" "+str(7)] = 705
edgW[str(8)+" "+str(10)] = 730
edgW[str(10)+" "+str(8)] = 730








edgW[str(8)+" "+str(9)] = 524
edgW[str(9)+" "+str(8)] = 524



def calcSum(res, edgW):

    sum = 0

    chosenEdges = {}
    res2 = []

    for entr in res:
        if(len(entr) == 2):
            continue

        first = entr[0]
        entr.append(first)
        res2.append(entr)

    for st in res2:
        edgK = []

        for i in range(len(st)):
            if i == len(st) - 1:
                break

            k = str(st[i]) + " " + str(st[i+1])
            k2 = str(st[i+1]) + " " + str(st[i])
            edgK.append(k)
            edgK.append(k2)

        minV = 100000
        minK = ""
        skip = False

        for i in range(len(edgK)):
            k = edgK[i]
            if k in chosenEdges:
                skip = True
                break

        if not skip:
            for i in range(len(edgK)):
                k = edgK[i]
                kVal = edgW[k]

                if minV > kVal:
                    minV = kVal
                    minK = k

            chosenEdges[minK] = 1
            sum = sum + minV

    print(sum)

res = entry_tarjan(GT)

calcSum(res, edgW)
#print(len(res))
p = 5