from copy import deepcopy

G = []
cycles = []

point_stack = []
marked = []
marked_stack = []

def tarjan(s, v):
    global cycles
    f = False
    point_stack.append(v)
    marked[v] = True
    marked_stack.append(v)
    for w in G[v]:
        if w<s:
            G[w] = 0
        elif w == s and len(point_stack) != 2:
            cycles.append(list(deepcopy(point_stack)))
            f = True
        elif marked[w] == False:
            g = tarjan(s,w)
            f = f or g
            
    if f == True:
        while marked_stack[len(marked_stack) - 1] != v:
            u = marked_stack.pop()
            marked[u] = False
        marked_stack.pop()
        marked[v] = False
        
    point_stack.pop()
    return f
        
def entry_tarjan(G_):
    global G, cycles, marked
    G = deepcopy(G_)

    marked = [False for x in range(0, len(G_))]
    
    for i in range(len(G)):
        tarjan(i, i)
        while marked_stack:
            u = marked_stack.pop()
            marked[u] = False
    
    return cycles


GT = [[],
      [2, 3, 4],
      [1, 3, 4],
      [1, 2],
      [1, 2]
    
    ]

GT = [[],
[2, 3, 4, 5, 7, 9, 10],
[1, 3, 4, 5, 6, 7, 8, 10],
[1, 10, 2, 4, 6, 7, 8],
[1, 2, 3, 6, 9],
[1, 2, 7, 9],
[10, 2, 3, 4, 7, 8, 9],
[1, 2, 3, 5, 6, 8],
[10, 2, 3, 6, 7, 9],
[1, 4, 5, 6, 8],
[3, 6, 8, 1, 2],
]

edgW = {}
edgW[str(1)+" "+str(2)] = 155
edgW[str(2)+" "+str(1)] = 155
edgW[str(1)+" "+str(3)] = 751
edgW[str(3)+" "+str(1)] = 751
edgW[str(1)+" "+str(4)] = 726
edgW[str(4)+" "+str(1)] = 726
edgW[str(1)+" "+str(5)] = 89
edgW[str(5)+" "+str(1)] = 89
edgW[str(1)+" "+str(7)] = 63
edgW[str(7)+" "+str(1)] = 63
edgW[str(1)+" "+str(9)] = 700
edgW[str(9)+" "+str(1)] = 700
edgW[str(1)+" "+str(10)] = 336
edgW[str(10)+" "+str(1)] = 336


edgW[str(2)+" "+str(3)] = 180
edgW[str(3)+" "+str(2)] = 180
edgW[str(2)+" "+str(4)] = 66
edgW[str(4)+" "+str(2)] = 66
edgW[str(2)+" "+str(5)] = 409
edgW[str(5)+" "+str(2)] = 409
edgW[str(2)+" "+str(6)] = 579
edgW[str(6)+" "+str(2)] = 579
edgW[str(2)+" "+str(7)] = 305
edgW[str(7)+" "+str(2)] = 305
edgW[str(2)+" "+str(8)] = 555
edgW[str(8)+" "+str(2)] = 555
edgW[str(2)+" "+str(10)] = 442
edgW[str(10)+" "+str(2)] = 442


edgW[str(3)+" "+str(10)] = 799
edgW[str(10)+" "+str(3)] = 799


edgW[str(3)+" "+str(4)] = 889
edgW[str(4)+" "+str(3)] = 889
edgW[str(3)+" "+str(6)] = 714
edgW[str(6)+" "+str(3)] = 714
edgW[str(3)+" "+str(7)] = 414
edgW[str(7)+" "+str(3)] = 414
edgW[str(3)+" "+str(8)] = 252
edgW[str(8)+" "+str(3)] = 252






edgW[str(4)+" "+str(6)] = 772
edgW[str(6)+" "+str(4)] = 772
edgW[str(4)+" "+str(9)] = 517
edgW[str(9)+" "+str(4)] = 517




edgW[str(5)+" "+str(7)] = 789
edgW[str(7)+" "+str(5)] = 789
edgW[str(5)+" "+str(9)] = 323
edgW[str(9)+" "+str(5)] = 323
edgW[str(6)+" "+str(10)] = 472
edgW[str(10)+" "+str(6)] = 472
edgW[str(6)+" "+str(7)] = 374
edgW[str(7)+" "+str(6)] = 374
edgW[str(6)+" "+str(8)] = 923
edgW[str(8)+" "+str(6)] = 923
edgW[str(6)+" "+str(9)] = 502
edgW[str(9)+" "+str(6)] = 502
edgW[str(7)+" "+str(8)] = 705
edgW[str(8)+" "+str(7)] = 705
edgW[str(8)+" "+str(10)] = 730
edgW[str(10)+" "+str(8)] = 730
edgW[str(8)+" "+str(9)] = 524
edgW[str(9)+" "+str(8)] = 524



def calcSum(res, edgW):

    sum = 0

    chosenEdges = {}
    res2 = []

    for entr in res:
        if(len(entr) == 2):
            continue

        first = entr[0]
        entr.append(first)
        res2.append(entr)

    for st in res2:
        edgK = []

        for i in range(len(st)):
            if i == len(st) - 1:
                break

            k = str(st[i]) + " " + str(st[i+1])
            k2 = str(st[i+1]) + " " + str(st[i])
            edgK.append(k)
            edgK.append(k2)

        minV = 100000
        minK = ""
        skip = False

        for i in range(len(edgK)):
            k = edgK[i]
            if k in chosenEdges:
                skip = True
                break

        if not skip:
            for i in range(len(edgK)):
                k = edgK[i]
                kVal = edgW[k]

                if minV > kVal:
                    minV = kVal
                    minK = k

            chosenEdges[minK] = 1
            sum = sum + minV

    print(sum)

res = entry_tarjan(GT)

calcSum(res, edgW)