﻿# -*- coding: utf-8 -*-

from ReadInput import readInput
from OutputResult import outputResult
from TarjIt import entry_tarjan, calcSum, resetGlobals, resetChosenEdgeMap, getCosenEdgeMap

from copy import deepcopy


f_path = input("Ievadiet pilnu celu uz ieejas failu: ")

parsedData = readInput(f_path)


GT = parsedData[0]
foundSum = 0

GLen = len(GT) - 1
cur = 3

foundCycles = []

while cur <= GLen:
    cycl = entry_tarjan(GT, cur)

    foundCycles = foundCycles + cycl
    #printList(foundCycles)
    if len(cycl) == 0:
        pass
    else:
        resetChosenEdgeMap()
        foundSum = calcSum(list(deepcopy(foundCycles)), parsedData[1])

    cur = cur + 1
    resetGlobals()

print(len(foundCycles))
outputResult(foundSum, getCosenEdgeMap())
